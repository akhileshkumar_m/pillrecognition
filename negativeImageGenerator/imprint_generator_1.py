import cv2
import numpy as np
import string
import argparse
import random
from packages import negativeImprintGenerator_1
from packages import removeImprint
import os

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", default=None,
	help="path to save the input and negative generated images")
ap.add_argument("-n", "--numberPill", default=10,
	help="path to save the input and negative generated images")
ap.add_argument("-i", "--imagePath", default="3.bmp",
	help="path to save the input and negative generated images")
ap.add_argument("-p", "--prefix", default=None,
	help="path to save the input and negative generated images")	
args = vars(ap.parse_args())	


NIG = negativeImprintGenerator_1 (args["imagePath"], (6,8))
inputImage = NIG.mainImage

if args["output"] is not None:
	cv2.imwrite (os.path.sep.join( [args["output"], args["prefix"]+"input_image"+".bmp"]), inputImage)

for img_count in range (int(args["numberPill"]) ):
	outImage = NIG.getNegImage("ADVANCE")
	if outImage is None:
		print (img_count)
	if args["output"] is not None:
		cv2.imwrite (  os.path.sep.join( [args["output"],args["prefix"]+"negative_image_"+str(img_count)+".bmp"]), outImage)
#	cv2.imshow('HSV_bitwise_or'+str(img_count), outImage)
	img_count += 1
#cv2.waitKey(0)
