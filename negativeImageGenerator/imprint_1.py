import cv2
import numpy as np
import string
import random
'''
print (string.ascii_letters)
print (string.digits)
'''
fontScaleList = [.8, .9, 1]
thicknessList = [2,3,4] 

img_in = cv2.imread('3.bmp')
cv2.imshow('Input', img_in)

def pastingObject (bgImage, extImage):
	ret, mask = cv2.threshold( extImage, 1, 255, 0 )
	invertMask = (255-mask)
	andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)
	orResult = andResult + extImage
	return orResult

for img_count in range(10):

	text = None
	for _ in range(6):
		l = random.choice((str(string.ascii_letters)+str(string.digits)))
		if text is None:
			text = str(l)
		else:
			text = text+str(l)
#	print (text)


	HSV_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)
	h_in,s_in,v_in = cv2.split(HSV_img_in)

	b_img,g_img,r_img = cv2.split(img_in)

	b_mean = b_img.mean()
	g_mean = g_img.mean()
	r_mean = r_img.mean()


	Temp = np.zeros((224, 224, 3), dtype="uint8")

	font = cv2.FONT_HERSHEY_SIMPLEX
	#text = "hello"
	fontScale = random.choice(fontScaleList)
	thickness = random.choice(thicknessList)
	
	
	# get boundary of this text
	textsize = cv2.getTextSize(text, font, fontScale, thickness)[0]

	# get coords based on boundary
	textX = (Temp.shape[1] - textsize[0]) / 2
	textY = (Temp.shape[0] + textsize[1]) / 2

	# add text centered on image
	cv2.putText(Temp, text, (int(textX), int(textY) ), font, fontScale, (b_mean, g_mean, r_mean), thickness)
	HSV_imprint = cv2.cvtColor(Temp, cv2.COLOR_BGR2HSV)
	h,s,v = cv2.split(HSV_imprint)

	h = np.zeros((224, 224), dtype="uint8")
	s = np.zeros((224, 224), dtype="uint8")

	kernel_emboss_1 = np.array([[0,1,0],
		                    [1,0,-1],
		                    [0,-1,0]])

	kernel_emboss_2 = np.array([[0,-1,0],
		                    [-1,0,1],
		                    [0,1,0]])

	v_1 = cv2.filter2D(v, -1, kernel_emboss_1)
	v_2 = cv2.filter2D(v, -1, kernel_emboss_2)
		              
	v_temp = v_2 - v_1

	HSV_imprint = cv2.merge((h ,s, v_temp ))

	HVS_or = HSV_img_in - HSV_imprint
	outimage = cv2.cvtColor(HVS_or, cv2.COLOR_HSV2BGR)
	cv2.imshow('HSV_bitwise_or'+str(img_count), outimage)
cv2.waitKey(0)
