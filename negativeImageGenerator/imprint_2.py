import cv2
import numpy as np
import string
import argparse
import random

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", default=None,
	help="path to save the input and negative generated images")
args = vars(ap.parse_args())	

def createImage (blurredImage, pillImage):
	ret, mask = cv2.threshold( pillImage, 1, 255, 0 )
	blurredImage [mask == 0] = 0
	return blurredImage

def pastingObject (bgImage, maskImage, extImage):
	maskImage = cv2.cvtColor(maskImage, cv2.COLOR_BGR2GRAY)
	ret, mask = cv2.threshold( maskImage, 1, 255, 0 )
	extImageAnd = cv2.bitwise_and(extImage, extImage, mask = mask)
	invertMask = (255-mask)
	andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)
	orResult = andResult + extImageAnd
	return orResult
def sp_noise(image,prob):

    output = np.zeros(image.shape,np.uint8)
    thres = 1 - prob 
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = int(mean_input[0]+5)
            elif rdn > thres:
                output[i][j] = int(mean_input[0])
            else:
                output[i][j] = image[i][j]
    return output

def pillDimension (image):
	ret, image = cv2.threshold( image, 1, 255, 0 )
	kernel = np.ones( ( 3, 3 ), np.uint8 )
	image = cv2.erode( image, kernel, iterations=3 )
	image = cv2.dilate( image, kernel, iterations=3 )
	contours = cv2.findContours( image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]

	return cv2.boundingRect(contours[0]) # x,y,w,h
	
'''
print (string.ascii_letters)
print (string.digits)
'''
#fontScaleList = [.8, .9, .5, .6]
fontScaleList = [.4, .7, 1, 1.5, 2 ]
thicknessList = [1, 2,3] 
textLengthList = [6,7,8,9]
fontList = [cv2.FONT_HERSHEY_SIMPLEX,
	    cv2.FONT_HERSHEY_PLAIN,
	    cv2.FONT_HERSHEY_DUPLEX,
	    cv2.FONT_HERSHEY_COMPLEX,
	    cv2.FONT_HERSHEY_TRIPLEX,
	    cv2.FONT_HERSHEY_COMPLEX_SMALL,
	    cv2.FONT_ITALIC,
	    ]

'''
enum
{
    FONT_HERSHEY_SIMPLEX = 0,
    FONT_HERSHEY_PLAIN = 1,
    FONT_HERSHEY_DUPLEX = 2,
    FONT_HERSHEY_COMPLEX = 3,
    FONT_HERSHEY_TRIPLEX = 4,
    FONT_HERSHEY_COMPLEX_SMALL = 5,
    FONT_HERSHEY_SCRIPT_SIMPLEX = 6,
    FONT_HERSHEY_SCRIPT_COMPLEX = 7,
    FONT_ITALIC = 16
}
'''
kernel = np.ones((5,5),np.uint8)
imprintList = str(string.ascii_letters[26:])+str(string.digits)+" "

img_in = cv2.imread('4.bmp')

img_in_copy = img_in.copy()
inputImage = img_in.copy()
img_in_gray = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY)
ret, mask_input = cv2.threshold( img_in_gray, 1, 255, 0 )

pillDims = pillDimension (mask_input)
print (pillDims)
mean_input = cv2.mean (img_in_gray, mask_input)


blurredImage = img_in.copy()
'''
kernel_blur = np.array([[1/8,  0, 1/8, 0,  1/8],
			[0,    0, 0,   0,  0  ],
			[1/8,  0, 0,   0,  1/8],
			[0,    0, 0,   0,  0  ],
			[1/8,  0, 1/8, 0,  1/8]])

'''
kernel_blur = np.array([[0,   0, 0, 1/4, 0, 0, 0  ],
			[0,   0, 0, 0,   0, 0, 0  ],
			[0,   0, 0, 0,   0, 0, 0  ],
			[1/4, 0, 0, 0,   0, 0, 1/4],
			[0,   0, 0, 0,   0, 0, 0  ],
			[0,   0, 0, 0,   0, 0, 0  ],
			[0,   0, 0, 1/4, 0, 0, 0  ]])

	
for _ in range (10):
	blurredImage = cv2.filter2D(blurredImage, -1, kernel_blur)
	
	
outputImage = createImage (blurredImage, img_in)

img_in_gray = cv2.cvtColor(img_in_copy, cv2.COLOR_BGR2GRAY)
adaptThresh = cv2.adaptiveThreshold(img_in_gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,41,1)


adaptThresh = 255-adaptThresh
cv2.imshow('adaptThresh', adaptThresh)

mask_input_erose = cv2.erode(mask_input,kernel,iterations = 2)
adaptThresh = cv2.bitwise_and(adaptThresh, adaptThresh, mask = mask_input_erose)
adaptThresh_imprint = cv2.dilate(adaptThresh,kernel,iterations = 2)

img_in_copy[adaptThresh_imprint > 0] = outputImage[adaptThresh_imprint > 0]

#cv2.imshow('adaptThresh_imprint', adaptThresh_imprint)
#cv2.imshow('img_in_copy', img_in_copy)

img_in_copy_2 = createImage (img_in_copy, img_in)
cv2.imshow('img_in_copy_2', img_in_copy_2)


imprintImage = createImage (img_in_copy_2.copy(), adaptThresh_imprint)
cv2.imshow ("imprint", imprintImage)

imprintImage = sp_noise(imprintImage,0.001)

imprintImage = createImage (imprintImage, adaptThresh_imprint)


cv2.imshow ("imprint_noise", imprintImage)

print ('imprintImage : ', imprintImage.shape)
print ('img_in_copy_2 : ', img_in_copy_2.shape)


img_in_copy_2 = pastingObject (img_in_copy_2, imprintImage.copy(), imprintImage.copy())
cv2.imshow('img_in_copy_3', img_in_copy_2)

for _ in range (4):
	img_in_copy_2 = cv2.blur (img_in_copy_2, (3,3),0)
#	cv2.imshow('img_in_copy_4', img_in_copy_2)
	img_in_copy_2 = createImage (img_in_copy_2, img_in)

cv2.imshow('img_in_copy_5', img_in_copy_2)
img_in_copy_2 = pastingObject (img_in.copy(), imprintImage.copy(), img_in_copy_2.copy())
cv2.imshow('img_in_copy_6', img_in_copy_2)

cv2.waitKey(0)

img_in = img_in_copy_2


#exit()
img_count = 0

if args["output"] is not None:
	cv2.imwrite (args["output"]+"input_image.bmp", inputImage)
while img_count < 10:
	text = None
	
	textLength = random.choice(textLengthList)
	for _ in range(textLength):
		l = random.choice((imprintList))
		if text is None:
			text = str(l)
		else:
			text = text+str(l)

	HSV_img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2HSV)
	h_in,s_in,v_in = cv2.split(HSV_img_in)

	b_img,g_img,r_img = cv2.split(img_in)

	b_mean = b_img.mean()
	g_mean = g_img.mean()
	r_mean = r_img.mean()


	Temp = np.zeros((224, 224, 3), dtype="uint8")

	font = random.choice(fontList)
	fontScale = random.choice(fontScaleList)
	thickness = random.choice(thicknessList)
	
	
	# get boundary of this text
	textsize = cv2.getTextSize(text, font, fontScale, thickness)[0]
	
	if (pillDims[2] < textsize[0] or pillDims[2]/2 > textsize[0]):
		continue
	# get coords based on boundary
	textX = (Temp.shape[1] - textsize[0]) / 2
	textY = (Temp.shape[0] + textsize[1]) / 2

	# add text centered on image
	cv2.putText(Temp, text, (int(textX), int(textY) ), font, fontScale, (b_mean, g_mean, r_mean), thickness)
	HSV_imprint = cv2.cvtColor(Temp, cv2.COLOR_BGR2HSV)
	h,s,v = cv2.split(HSV_imprint)

	h = np.zeros((224, 224), dtype="uint8")
	s = np.zeros((224, 224), dtype="uint8")

	kernel_emboss_1 = np.array([[0,1,0],
		                    [1,0,-1],
		                    [0,-1,0]])

	kernel_emboss_2 = np.array([[0,-1,0],
		                    [-1,0,1],
		                    [0,1,0]])

	v_1 = cv2.filter2D(v, -1, kernel_emboss_1)
	v_2 = cv2.filter2D(v, -1, kernel_emboss_2)
		              
	v_temp = v_2 - v_1

	HSV_imprint = cv2.merge((h ,s, v_temp ))

	HSV_or = HSV_img_in - HSV_imprint
	outimage = cv2.cvtColor(HSV_or, cv2.COLOR_HSV2BGR)
	if args["output"] is not None:
		cv2.imwrite (args["output"]+"negative_image_"+str(img_count)+".bmp", outimage)
	cv2.imshow('HSV_bitwise_or'+str(img_count), outimage)
	img_count += 1
cv2.waitKey(0)
