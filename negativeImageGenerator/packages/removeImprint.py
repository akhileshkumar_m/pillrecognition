import cv2
import numpy as np
import string
import argparse
import random


class removeImprint:
	def __init__ (self, image):
		self.inputImage = image
		self.inputImageGRAY = cv2.cvtColor(self.inputImage, cv2.COLOR_BGR2GRAY)
		self.kernel = np.ones((5,5),np.uint8)
		self.kernel_blur = np.array([[0,   0, 0, 1/4, 0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[1/4, 0, 0, 0,   0, 0, 1/4],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 0,   0, 0, 0  ],
					[0,   0, 0, 1/4, 0, 0, 0  ]])
					
	def createImage (self, blurredImage, pillImage):
		ret, mask = cv2.threshold( pillImage, 1, 255, 0 )
		blurredImage [mask == 0] = 0
		return blurredImage

	def pastingObject (self, bgImage, maskImage, extImage):
		maskImage = cv2.cvtColor(maskImage, cv2.COLOR_BGR2GRAY)
		ret, mask = cv2.threshold( maskImage, 1, 255, 0 )
		extImageAnd = cv2.bitwise_and(extImage, extImage, mask = mask)
		invertMask = (255-mask)
		andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)
		orResult = andResult + extImageAnd
		return orResult
	def sp_noise(self, image,prob):

		output = np.zeros(image.shape,np.uint8)
		thres = 1 - prob 
		for i in range(image.shape[0]):
			for j in range(image.shape[1]):
				rdn = random.random()
				if rdn < prob:
					output[i][j] = int(self.mean_input[0]+5)
				elif rdn > thres:
					output[i][j] = int(self.mean_input[0])
				else:
					output[i][j] = image[i][j]
		return output
	
	def getImage (self):
		inputImageCopy = self.inputImage.copy()
		inputImage = self.inputImage.copy()
		ret, maskInput = cv2.threshold( self.inputImageGRAY, 1, 255, 0 )

		self.mean_input = cv2.mean (self.inputImageGRAY, maskInput)

		blurredImage = self.inputImage.copy()

		for _ in range (10):
			blurredImage = cv2.filter2D(blurredImage, -1, self.kernel_blur)
	
		sharpBlurredInput = self.createImage (blurredImage, self.inputImage)

		# finding imprint area
		adaptThresh = cv2.adaptiveThreshold(self.inputImageGRAY, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
							cv2.THRESH_BINARY,41,1)

		adaptThreshInvert = 255-adaptThresh

		maskInputErose = cv2.erode(maskInput, self.kernel,iterations = 2)
		adaptThreshInvert = cv2.bitwise_and(adaptThreshInvert, adaptThreshInvert, mask = maskInputErose)
		adaptThresh_imprint = cv2.dilate(adaptThresh, self.kernel,iterations = 2)
		
		# replacing imprint area with the blurred image
		inputImageCopy[adaptThresh_imprint > 0] = sharpBlurredInput[adaptThresh_imprint > 0]
		
		
		img_in_copy_2 = self.createImage (inputImageCopy, self.inputImage)
		
		imprintImage  = self.createImage (img_in_copy_2.copy(), adaptThresh_imprint)
		imprintImage  = self.sp_noise(imprintImage,0.001)
		imprintImage  = self.createImage (imprintImage, adaptThresh_imprint)
		img_in_copy_2 = self.pastingObject (img_in_copy_2, imprintImage.copy(), imprintImage.copy())

		for _ in range (4):
			img_in_copy_2 = cv2.blur (img_in_copy_2, (3,3),0)
			img_in_copy_2 = self.createImage (img_in_copy_2, self.inputImage)

		img_in_copy_2 = self.pastingObject (self.inputImage.copy(), imprintImage.copy(), img_in_copy_2.copy())
		return img_in_copy_2
