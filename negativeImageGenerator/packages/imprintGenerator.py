import cv2
import numpy as np
import string
import argparse
import random



'''
enum
{
    FONT_HERSHEY_SIMPLEX = 0,
    FONT_HERSHEY_PLAIN = 1,
    FONT_HERSHEY_DUPLEX = 2,
    FONT_HERSHEY_COMPLEX = 3,
    FONT_HERSHEY_TRIPLEX = 4,
    FONT_HERSHEY_COMPLEX_SMALL = 5,
    FONT_HERSHEY_SCRIPT_SIMPLEX = 6,
    FONT_HERSHEY_SCRIPT_COMPLEX = 7,
    FONT_ITALIC = 16
}
'''
	    
class negativeImprintGenerator:
	def __init__ (self, inputImage, pixelMargin=10, textLengthRange=(6,9)):
		self.inputImage = inputImage
		inputImageGray = cv2.cvtColor(inputImage, cv2.COLOR_BGR2GRAY)
		ret, mask_input = cv2.threshold( inputImageGray, 1, 255, 0 )
		ret, mask_input = cv2.threshold( mask_input, 1, 255, 0 )
		kernel = np.ones( ( 3, 3 ), np.uint8 )
		image = cv2.erode( mask_input, kernel, iterations=3 )
		image = cv2.dilate( mask_input, kernel, iterations=3 )
		contours = cv2.findContours( image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]
		self.pillDims = cv2.boundingRect(contours[0]) # x,y,w,h
		self.pixelMargin = pixelMargin
		self.fontScaleList = [.4, .7, 1, 1.5, 2 ]
		self.thicknessList = [1, 2,3] 
		self.textLengthList = list(np.arange(textLengthRange[0], textLengthRange[1]) )
#		self.textLengthList = [6,7,8,9]
		self.fontList = [cv2.FONT_HERSHEY_SIMPLEX,
			    cv2.FONT_HERSHEY_PLAIN,
			    cv2.FONT_HERSHEY_DUPLEX,
			    cv2.FONT_HERSHEY_COMPLEX,
			    cv2.FONT_HERSHEY_TRIPLEX,
			    cv2.FONT_HERSHEY_COMPLEX_SMALL,
			    cv2.FONT_ITALIC,
			    ]
		self.imprintList = str(string.ascii_letters[26:])+str(string.digits)+" "
	
	
	def getNegImage (self, trueImprint):
		while True:
			text = None
			textLength = random.choice(self.textLengthList)
			for _ in range(textLength):
				l = random.choice((self.imprintList))
				if text is None:
					text = str(l)
				else:
					text = text+str(l)

			if trueImprint is text:
				continue

			HSV_inputImage = cv2.cvtColor(self.inputImage, cv2.COLOR_BGR2HSV)
			h_in,s_in,v_in = cv2.split(HSV_inputImage)

			b_img,g_img,r_img = cv2.split(self.inputImage)

			b_mean = b_img.mean()
			g_mean = g_img.mean()
			r_mean = r_img.mean()


			Temp = np.zeros((224, 224, 3), dtype="uint8")

			font = random.choice(self.fontList)
			fontScale = random.choice(self.fontScaleList)
			thickness = random.choice(self.thicknessList)
	
	
			# get boundary of this text
			textsize = cv2.getTextSize(text, font, fontScale, thickness)[0]
			print (	type(self.pillDims))
			print (	type(self.pillDims))

			if (self.pillDims[2]-self.pixelMargin < textsize[0] or self.pillDims[2]/2 > textsize[0]):
				continue
			# get coords based on boundary
			textX = (Temp.shape[1] - textsize[0]) / 2
			textY = (Temp.shape[0] + textsize[1]) / 2

			# add text centered on image
			cv2.putText(Temp, text, (int(textX), int(textY) ), font, fontScale, (b_mean, g_mean, r_mean), thickness)
			HSV_imprint = cv2.cvtColor(Temp, cv2.COLOR_BGR2HSV)
			h,s,v = cv2.split(HSV_imprint)

			h = np.zeros((224, 224), dtype="uint8")
			s = np.zeros((224, 224), dtype="uint8")

			kernel_emboss_1 = np.array([[0,1,0],
						    [1,0,-1],
						    [0,-1,0]])

			kernel_emboss_2 = np.array([[0,-1,0],
						    [-1,0,1],
						    [0,1,0]])

			v_1 = cv2.filter2D(v, -1, kernel_emboss_1)
			v_2 = cv2.filter2D(v, -1, kernel_emboss_2)
					      
			v_temp = v_2 - v_1

			HSV_imprint = cv2.merge((h ,s, v_temp ))

			HSV_or = HSV_inputImage - HSV_imprint
			outImage = cv2.cvtColor(HSV_or, cv2.COLOR_HSV2BGR)
#			cv2.imshow('class_HSV_bitwise_or', outImage)
#			cv2.waitKey(0)
			return outImage
