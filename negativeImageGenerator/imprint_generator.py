import cv2
import numpy as np
import string
import argparse
import random
from packages import negativeImprintGenerator
from packages import removeImprint

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", default=None,
	help="path to save the input and negative generated images")
ap.add_argument("-n", "--numberPill", default=10,
	help="path to save the input and negative generated images")
ap.add_argument("-i", "--imagePath", default="3.bmp",
	help="path to save the input and negative generated images")
	
args = vars(ap.parse_args())	
inputImage = cv2.imread(args["imagePath"])

RI = removeImprint(inputImage)
planeImage = RI.getImage()

cv2.imshow ('inputImage', inputImage)
cv2.imshow ('planeImage', planeImage)
cv2.waitKey(0)

NIG = negativeImprintGenerator (planeImage, (6,8))

if args["output"] is not None:
	cv2.imwrite (args["output"]+"input_image.bmp", inputImage)

for img_count in range (int(args["numberPill"]) ):
	outImage = NIG.getNegImage("ADVANCE")
	if outImage is None:
		print (img_count)
	if args["output"] is not None:
		cv2.imwrite (args["output"]+"negative_image_"+str(img_count)+".bmp", outImage)
	cv2.imshow('HSV_bitwise_or'+str(img_count), outImage)
	img_count += 1
cv2.waitKey(0)
