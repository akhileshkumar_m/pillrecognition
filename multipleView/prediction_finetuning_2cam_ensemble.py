# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorNImagesResizedMirror2Cam as EP
#from pyimagesearch.preprocessing import ExtractPreprocessorNImagesResizedMirror2CamNotMiddle as EP
import sys
import imutils
#import progressbar
#import random
import os
import cv2
import pickle
import h5py
from keras.models import load_model
import datetime

### Multiprocessing
import multiprocessing as mp
from multiprocessing import Queue, Value


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-m2", "--model_2", required=True,
	help="path to model")
ap.add_argument("-r", "--ref", default=None,
	help="path to model")
args = vars(ap.parse_args())



model_1 = load_model(args["model"])
model_2 = load_model(args["model_2"])

fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
print ('labels : ',labels)
f.close()


refImages = [None]*(len(labels))
refImgCount = 0
for lb in labels:
	refPath = os.path.sep.join([args['ref'], lb])
	refImgPath = list(paths.list_images(refPath))
	refImages[refImgCount] = cv2.imread(refImgPath[0])
	refImgCount += 1



crop_w = 224
crop_h = 224
normalizeCam = False
MARGIN = 50

camFront = cv2.VideoCapture (0)
if not camFront.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

camTop = cv2.VideoCapture (1)
if not camTop.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

camFront.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
camFront.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)
camTop.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
camTop.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

#cv2.namedWindow ('Full Image front', cv2.WINDOW_NORMAL)
#cv2.resizeWindow ('Full Image front', int(1280/2), int(720/2))
#cv2.namedWindow ('Full Image top', cv2.WINDOW_NORMAL)
#cv2.resizeWindow ('Full Image top', int(1280/2), int(720/2))



imageCount = 0
thresVal = 10

def imshowByN (name, image, by=1):
	cv2.imshow (name, cv2.resize(image, (int(image.shape[1]/by), int(image.shape[0]/by)), interpolation = cv2.INTER_CUBIC))

def takeBackground ():
	while True:
		ret, image_front = camFront.read()
		if ret is False:
			print ("image not found")
			exit()

		ret, image_top = camTop.read()
		if ret is False:
			print ("image not found")
			exit()

		image_front = imutils.rotate(image_front, 180)
		#cv2.imshow ('Full Image front', image_front)
		#cv2.imshow ('Full Image top', image_top)
		#imshowByN ('Full Image front', image_front, 2)
		#imshowByN ('Full Image top', image_top, 2)
		imshowByN ('fullImages', np.concatenate((image_front, image_top), axis=1), 2)

		k = cv2.waitKey(1)
		if chr(k&255) is 'q':
			exit()
		elif chr(k&255) is 'b':
			return image_front, image_top

image_front, image_top = takeBackground()



reqImgW = int(len(image_front[0]))
reqImgH = int(len(image_front)/2)+30

#cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
#cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))

imgX, imgY = int(image_front.shape[1]/2), int(image_front.shape[0]/2)

croppedImageFront = image_front [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
croppedImageTop = image_top [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]

first_gray_front = cv2.cvtColor(croppedImageFront, cv2.COLOR_BGR2GRAY)
first_gray_top = cv2.cvtColor(croppedImageTop, cv2.COLOR_BGR2GRAY)
#extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
def predictionDisplay (preds, point):
	canvas = np.zeros((35*20+5, 350*2, 3), dtype="uint8")
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
	#		print (cl)
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		#if cl < 15:
		w = int (score[cl]*3 )
		if cl == pred:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (255, 0, 0), -1)
		else:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10+400*int(cl/20), (int(cl%20) * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	#os.system("espeak labels[int(pred)]")
	cv2.putText(tempImageFront, labels[int(pred)]+' '+str(best_score), point, 0, 0.8, (0, 0, 255), 4, cv2.LINE_AA)
	#cv2.imshow ('probabilities', canvas)
	#cv2.imshow ('frame', fullImage)
	#cv2.resizeWindow ('frame', int(1280/4), int(720/4))
	return pred, best_score

def fineTunePrediction (image):
	image = np.expand_dims(image, axis=0)
	features = image
	preds_1 = model_1.predict(features, verbose=0)
	preds_2 = model_2.predict(features, verbose=0)
	preds = (preds_1+preds_2)/2
	_, _ = predictionDisplay (preds_1, (300, 30))
	_, _ = predictionDisplay (preds_2, (300, 60))
	pred, best_score = predictionDisplay (preds,   (300, 90))
	#cv2.imshow ('reference image', refImages[int(pred)])

	imshowByN ('reference image', refImages[int(pred)])



while True:
	ret, image_front = camFront.read()
	if ret is False:
		print ("image not found")
		exit()

	ret, image_top = camTop.read()
	if ret is False:
		print ("image not found")
		exit()

	image_front = imutils.rotate(image_front, 180)

	tempImageFront = image_front.copy()
	tempImageFront[imgY-int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageFront[imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageFront[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX+int(reqImgW/2)-1] = 255
	tempImageFront[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2)] = 255


	tempImageTop = image_top.copy()
	tempImageTop[imgY-int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageTop[imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageTop[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX+int(reqImgW/2)-1] = 255
	tempImageTop[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2)] = 255


	image_front = image_front [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
	nextBgImageFront = image_front.copy()

	image_top = image_top [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
	nextBgImageTop = image_top.copy()




	thresholdFullImage, _, extractedImageList, biggestPillImage = extImage.preprocess (image_front, image_top, normalizeCam)
	totalNumImg = len(extractedImageList)

	# cv2.imshow ('threshold Image', thresholdFullImage)
	# cv2.imshow ('Full Image top', tempImageTop)

	imshowByN ('threshold Image', thresholdFullImage, 2)

	if biggestPillImage is not None:
		#cv2.imshow ('biggestPillImage', biggestPillImage)
		imshowByN ('biggestPillImage', biggestPillImage)
		if np.asarray (biggestPillImage).shape == (224, 224, 3):
			startTime = datetime.datetime.now()
			fineTunePrediction(biggestPillImage)
			endTime = datetime.datetime.now()

	if len(extractedImageList) is not 0:
		#cv2.imshow ('croppedImages', np.concatenate(extractedImageList, axis=1))
		imshowByN ('croppedImages', np.concatenate(extractedImageList, axis=1), 1)

	# imshowByN ('Full Image top', tempImageTop, 2)
	# imshowByN ('Full Image front', tempImageFront, 2)
	imshowByN ('fullImages', np.concatenate((tempImageTop, tempImageFront), axis=1), 2)

	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'b':
		first_gray_front = cv2.cvtColor(nextBgImageFront, cv2.COLOR_BGR2GRAY)
		first_gray_top = cv2.cvtColor(nextBgImageTop, cv2.COLOR_BGR2GRAY)


		extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
		cv2.destroyAllWindows()

		#cv2.namedWindow ('Full Image front', cv2.WINDOW_NORMAL)
		#cv2.resizeWindow ('Full Image front', int(1280/2), int(720/2))
		#cv2.namedWindow ('Full Image top', cv2.WINDOW_NORMAL)
		#cv2.resizeWindow ('Full Image top', int(1280/2), int(720/2))
		#cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
		#cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))

	elif chr(k&255) is 'v':
		thresVal = int(input("Enter threshold : "))
		extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)


	elif chr(k&255) is 'n':
		normalizeCam = not normalizeCam
		print (normalizeCam)

	'''
	elif chr(k&255) is 'n':
		if (sys.version_info > (3, 0)):
			dirName = input ("Inter directory name after {} :".format(args['output']))
		else:
			dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], 'full', dirName, dirName+'_'+str(imageCount)+"_full.bmp"]), fullImage)
			cv2.imwrite (os.path.sep.join([args['output'], 'extracted', dirName, dirName+'_'+str(imageCount)+".bmp"]), extractedImage)
			imageCount += 1
	'''
