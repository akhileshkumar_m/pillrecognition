# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorNImagesResizedMirror as EP
import sys
import imutils
# for prediction
#from keras.applications import VGG16
#from keras.applications import imagenet_utils
#from keras.preprocessing.image import img_to_array
#from keras.preprocessing.image import load_img
#from sklearn.preprocessing import LabelEncoder
#from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import random
import os
import cv2
#from sklearn.linear_model import LogisticRegression
#from sklearn.model_selection import GridSearchCV
#from sklearn.metrics import classification_report
#import argparse
import pickle
import h5py
#from pyimagesearch.preprocessing import ExtractPreprocessor
from keras.models import load_model
import datetime


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-r", "--ref", default=None,
	help="path to model")
args = vars(ap.parse_args())



model_1 = load_model(args["model"])

fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
print ('labels : ',labels)
f.close()


refImages = [None]*(len(labels))
refImgCount = 0
for lb in labels:
	refPath = os.path.sep.join([args['ref'], lb])
	refImgPath = list(paths.list_images(refPath))
	print (refImgPath[0])
	refImages[refImgCount] = cv2.imread(refImgPath[0])
	refImgCount += 1



crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

cv2.namedWindow ('Full Image', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('Full Image', int(1280/2), int(720/2))



imageCount = 0
thresVal = 10
def takeBackground ():
	while True:
		ret, image = cam.read()
		if ret is False:
			print ("image not found")
			exit()
		image = imutils.rotate(image, 180)
		cv2.imshow ('Full Image', image)
		k = cv2.waitKey(1)
		if chr(k&255) is 'q':
			exit()
		elif chr(k&255) is 'b':
			return image

image = takeBackground()



reqImgW = int(len(image[0]))
reqImgH = int(len(image)/2)+30

cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))

imgX, imgY = int(image.shape[1]/2), int(image.shape[0]/2)

croppedImage = image [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
#extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)

def fineTunePrediction (image):
	'''
	fullImage = image.copy()

	#image = image [int(imgY)-int(crop_h/2)-MARGIN:int(imgY)+int(crop_h/2)+MARGIN, int(imgX)-int(crop_w/2)-MARGIN:int(imgX)+int(crop_w/2)+MARGIN]

	status, extractedImage = extImage.preprocess (image)
	cv2.imshow("extractedImage", extractedImage)
	image = extractedImage.copy()
	'''

	#canvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
	canvas = np.zeros((35*20+5, 350*2, 3), dtype="uint8")
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)

	features = image
	#preds = model_1.predict_proba(features, verbose=0)
	preds = model_1.predict(features, verbose=0)
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
	#		print (cl)
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		#if cl < 15:
		w = int (score[cl]*3 )
		if cl == pred:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (255, 0, 0), -1)
		else:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10+400*int(cl/20), (int(cl%20) * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	#os.system("espeak labels[int(pred)]")
	cv2.putText(tempImage, labels[int(pred)]+' '+str(best_score), (300, 50), 0, 0.8, (0, 0, 255), 4, cv2.LINE_AA)

	#cv2.imshow ('croppedImage', croppedImage)
	#cv2.imshow ('probabilities', canvas)
	#cv2.imshow ('frame', fullImage)
	#cv2.resizeWindow ('frame', int(1280/4), int(720/4))
	cv2.imshow ('reference image', refImages[int(pred)])
	'''
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		return exit()
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_full.bmp"]),        fullImage)
			#if extractedImage is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+".bmp"]),             tempImage)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_probability.bmp"]), canvas)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_cropped.bmp"]),     croppedImage)
			imageCount += 1
	'''
	return pred

while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()

	image = imutils.rotate(image, 180)
	tempImage = image.copy()

	tempImage[imgY-int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImage[imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImage[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX+int(reqImgW/2)-1] = 255
	tempImage[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2)] = 255

	fullImage = image.copy()
	image = image [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
	nextBgImage = image.copy()
	thresholdFullImage, thresholdFullImagetTop, extractedImageList, biggestPillImage = extImage.preprocess (image)
	totalNumImg = len(extractedImageList)

	cv2.imshow ('threshold Image', thresholdFullImage)
	cv2.imshow ('threshold Image Top', thresholdFullImagetTop)
	if biggestPillImage is not None:
		cv2.imshow ('biggestPillImage', biggestPillImage)
		if np.asarray (biggestPillImage).shape == (224, 224, 3):
			startTime = datetime.datetime.now()
			fineTunePrediction(biggestPillImage)
			endTime = datetime.datetime.now()

			#print ("time taken by prediction : {}".format(endTime-startTime))
		#else:
		#	print ('shape is {}'.format(np.asarray (biggestPillImage).shape))

	if len(extractedImageList) is not 0:
		cv2.imshow ('croppedImages', np.concatenate(extractedImageList, axis=1))

	'''
	for ind, extractedImage in enumerate(extractedImageList):

		#_, stdDev = cv2.meanStdDev (extractedImage)
		#cv2.putText(extractedImage, str(stdDev), (30, 30), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
		cv2.imshow ('Cropped_Image_'+str(ind), extractedImage)
		# extractedImage = cv2.cvtColor(extractedImage, cv2.COLOR_BGR2GRAY)
		# extractedImage = cv2.adaptiveThreshold(extractedImage, 255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
		# cv2.imshow ('adaptive_Cropped_Image_'+str(ind), extractedImage)
	'''

	#cv2.imshow ('threshold Image', extractedImageList[-1])
	cv2.imshow ('Full Image', tempImage)
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'b':
		first_gray = cv2.cvtColor(nextBgImage, cv2.COLOR_BGR2GRAY)
		extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)
		cv2.destroyAllWindows()
		cv2.namedWindow ('Full Image', cv2.WINDOW_NORMAL)
		cv2.resizeWindow ('Full Image', int(1280/2), int(720/2))
		cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
		cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))
	elif chr(k&255) is 'v':
		#first_gray = cv2.cvtColor(nextBgImage, cv2.COLOR_BGR2GRAY)
		thresVal = int(input("Enter threshold : "))
		extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)
	elif chr(k&255) is 'n':
		if (sys.version_info > (3, 0)):
			dirName = input ("Inter directory name after {} :".format(args['output']))
		else:
			dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], 'full', dirName, dirName+'_'+str(imageCount)+"_full.bmp"]), fullImage)
			cv2.imwrite (os.path.sep.join([args['output'], 'extracted', dirName, dirName+'_'+str(imageCount)+".bmp"]), extractedImage)
			imageCount += 1
