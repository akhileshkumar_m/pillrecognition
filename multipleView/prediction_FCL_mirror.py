# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorNImagesResizedMirror as EP
import sys
import imutils
#import progressbar
#import random
import os
import cv2
import pickle
import h5py
from keras.models import load_model
import datetime
from keras.applications import VGG16
### Multiprocessing
import multiprocessing as mp
from multiprocessing import Queue, Value


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-r", "--ref", default=None,
	help="path to model")
args = vars(ap.parse_args())



# load the VGG16 network
print("[INFO] loading network...")
model = VGG16(weights="imagenet", include_top=False)

model_1 = load_model(args["model"])

fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
print ('labels : ',labels)
f.close()


refImages = [None]*(len(labels))
refImgCount = 0
for lb in labels:
	refPath = os.path.sep.join([args['ref'], lb])
	refImgPath = list(paths.list_images(refPath))
	refImages[refImgCount] = cv2.imread(refImgPath[0])
	refImgCount += 1



crop_w = 224
crop_h = 224

MARGIN = 50

camFront = cv2.VideoCapture (0)
if not camFront.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')


camFront.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
camFront.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)



imageCount = 5
thresVal = 10
def imshowByN (name, image, by=2):
	if by > 1:
		interpolation = cv2.INTER_AREA
	else:
		interpolation = cv2.INTER_CUBIC
	cv2.imshow (name, cv2.resize(image, (int(image.shape[1]/by), int(image.shape[0]/by)), interpolation = interpolation))

def takeBackground ():
	while True:
		ret, image_front = camFront.read()
		if ret is False:
			print ("image not found")
			exit()

		image_front = imutils.rotate(image_front, 180)
		imshowByN ('Full Image front', image_front, 3)

		k = cv2.waitKey(1)
		if chr(k&255) is 'q':
			exit()
		elif chr(k&255) is 'b':
			return image_front

image_front= takeBackground()



reqImgW = int(len(image_front[0]))
reqImgH = int(len(image_front)/2)+30
'''
cv2.namedWindow ('threshold Image front', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('threshold Image front', int(reqImgW/2), int(reqImgH/2))
cv2.namedWindow ('threshold Image top', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('threshold Image top', int(reqImgW/2), int(reqImgH/2))
'''
imgX, imgY = int(image_front.shape[1]/2), int(image_front.shape[0]/2)

croppedImageFront = image_front [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]

first_gray_front = cv2.cvtColor(croppedImageFront, cv2.COLOR_BGR2GRAY)
#extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
#extImage = EP(GRAYBaseFrameFrontCam=first_gray_front, GRAYBaseFrameTopCam=first_gray_top, thresholdValue=thresVal, H=crop_h, W=crop_w)
extImage = EP(GRAYBaseFrame = first_gray_front, thresholdValue = thresVal, H=crop_h, W=crop_w)
def fineTunePrediction (image):
	#canvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
	canvas = np.zeros((35*20+5, 350*2, 3), dtype="uint8")
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)

	features = model.predict(image, batch_size=1, verbose=0)
	# reshape the features so that each image is represented by
	# a flattened feature vector of the `MaxPooling2D` outputs
	features = features.reshape((1, 512 * 7 * 7))

	#features = image
	#preds = model_1.predict_proba(features, verbose=0)
	preds = model_1.predict(features, verbose=0)
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
	#		print (cl)
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		#if cl < 15:
		w = int (score[cl]*3 )
		if cl == pred:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (255, 0, 0), -1)
		else:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10+400*int(cl/20), (int(cl%20) * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	#os.system("espeak labels[int(pred)]")
	cv2.putText(tempImageFront, labels[int(pred)]+' '+str(best_score), (300, 50), 0, 0.8, (0, 0, 255), 4, cv2.LINE_AA)

	#cv2.imshow ('croppedImage', croppedImage)
	#cv2.imshow ('probabilities', canvas)
	#cv2.imshow ('frame', fullImage)
	#cv2.resizeWindow ('frame', int(1280/4), int(720/4))
	#cv2.imshow ('reference image', refImages[int(pred)])
	imshowByN ('reference image', refImages[int(pred)], 1)
	return pred

normalizeCam = True
while True:
	ret, image_front = camFront.read()
	if ret is False:
		print ("image not found")
		exit()


	image_front = imutils.rotate(image_front, 180)

	tempImageFront = image_front.copy()
	tempImageFront[imgY-int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageFront[imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImageFront[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX+int(reqImgW/2)-1] = 255
	tempImageFront[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2)] = 255

	image_front = image_front [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
	nextBgImageFront = image_front.copy()


	thresholdFullImageFront, extractedImageList, biggestPillImage = extImage.preprocess (
				image_front)
	totalNumImg = len(extractedImageList)

	imshowByN ('threshold Image front', thresholdFullImageFront, 3)

	if biggestPillImage is not None:
		#cv2.imshow ('biggestPillImage', biggestPillImage)
		imshowByN ('biggestPillImage', biggestPillImage, 1)
		if np.asarray (biggestPillImage).shape == (224, 224, 3):
			startTime = datetime.datetime.now()
			fineTunePrediction(biggestPillImage)
			endTime = datetime.datetime.now()

	if len(extractedImageList) is not 0:
		#cv2.imshow ('croppedImages', np.concatenate(extractedImageList, axis=1))
		imshowByN ('croppedImages', np.concatenate(extractedImageList, axis=1), 2)
	#cv2.imshow ('Full Image front', tempImageFront)
	imshowByN ('Full Image front', tempImageFront, 2)
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'b':
		first_gray_front = cv2.cvtColor(nextBgImageFront, cv2.COLOR_BGR2GRAY)
		extImage = EP(GRAYBaseFrame = first_gray_front, thresholdValue = thresVal, H=crop_h, W=crop_w)
		cv2.destroyAllWindows()

	elif chr(k&255) is 'v':
		thresVal = int(input("Enter threshold : "))
		extImage = EP(GRAYBaseFrame = first_gray_front, thresholdValue = thresVal, H=crop_h, W=crop_w)

	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_top.bmp"]), fullImage)
			#cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+".bmp"]), extractedImage)
			imageCount += 1
