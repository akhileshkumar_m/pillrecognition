# import the necessary packages
import numpy as np
import cv2
#import copy
class ExtractPreprocessorNImagesResizedMirror_1:
	def __init__(self, GRAYBaseFrame, thresholdValue, H, W, NumPills=3):
		self.GRAYBaseFrame = GRAYBaseFrame
		self.thresholdValue = thresholdValue
		self.H = H
		self.W = W
		self.initialWidth  = len(GRAYBaseFrame[0])
		self.initialHeight = len(GRAYBaseFrame)
		self.NumPills = NumPills
		self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
		self.fgbg = cv2.createBackgroundSubtractorMOG2(history=1, varThreshold = 10, detectShadows = True)
		self.fgbg_1 = None

	def biggestPill (self, extractedImageList):
		largest_area = 0
		largest_contour_index = None
		for ind, extractedImage in enumerate(extractedImageList):
			extractedImageGray = cv2.cvtColor(extractedImage, cv2.COLOR_BGR2GRAY)
			ret, thresholded_frame = cv2.threshold( extractedImageGray, 1, 255, 0 )
			contours = cv2.findContours( thresholded_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]
			if len(contours) is not 0:
				cnt = contours[0]
				area = cv2.contourArea(cnt)
				if( area > largest_area):
					x,y,w,h = cv2.boundingRect(cnt)
					largest_area = area;
					largest_contour = cnt
					largest_contour_index=ind;
		return largest_contour_index

	def imshow (self, name, image, by=2):
		cv2.imshow (name, cv2.resize(image, (int(image.shape[1]/by), int(image.shape[0]/by)), interpolation = cv2.INTER_CUBIC))

	def preprocess(self, RGBFrame):
		kernel = np.ones( ( 3, 3 ), np.uint8 )
		#_ = self.fgbg.apply(self.GRAYBaseFrame)
		#fgbg_1 = self.fgbg.copy()
		#self.fgbg_1 = copy.copy(self.fgbg)
		#print ("passed")
		#fgmask = self.fgbg.apply(RGBFrame)
		#fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, self.kernel)


		largest_area = 0
		divideBy = 3
		GRAYFrame = cv2.cvtColor(RGBFrame, cv2.COLOR_BGR2GRAY)
		'''
		_ = self.fgbg.apply(self.GRAYBaseFrame)
		fgmask = self.fgbg.apply(GRAYFrame)
		fgmask = cv2.erode( fgmask, kernel, iterations=3 )
		fgmask = cv2.dilate( fgmask, kernel, iterations=3 )

		#fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, self.kernel)
		cv2.imshow ('fgmask', cv2.resize(fgmask, (int(fgmask.shape[1]/divideBy), int(fgmask.shape[0]/divideBy)), interpolation = cv2.INTER_CUBIC))
		'''

		subImg = cv2.subtract( GRAYFrame, self.GRAYBaseFrame)
		subImg_temp = cv2.subtract( self.GRAYBaseFrame, GRAYFrame)

		ret, thresholded_frame_1 = cv2.threshold( subImg, self.thresholdValue, 255, 0 )
		#ret, thresholded_frame_2 = cv2.threshold( subImg_temp, 5, 255, 0 )

		opening_1 = cv2.morphologyEx(thresholded_frame_1, cv2.MORPH_OPEN, kernel, iterations=3)
		#opening_2 = cv2.morphologyEx(thresholded_frame_2, cv2.MORPH_OPEN, kernel, iterations=3)

		#self.imshow ('opening_2', opening_2)
#		self.imshow ('opening_1', opening_1)

		added_opening = opening_1

		#added_opening = cv2.dilate( added_opening, kernel, iterations=3 )
		#closing = cv2.morphologyEx(added_opening, cv2.MORPH_CLOSE, kernel, iterations=3)
		closing = cv2.morphologyEx(added_opening, cv2.MORPH_OPEN, kernel, iterations=5)

		#closing = cv2.erode( closing, kernel, iterations=3 )
#		self.imshow ('closing', closing)


		#closing = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=3)
		#closing = cv2.morphologyEx(closing	, cv2.MORPH_OPEN, kernel, iterations=3)

		eroded_frame_2 = closing.copy()


		res = cv2.bitwise_and(RGBFrame, RGBFrame, mask = eroded_frame_2)
		res_x_cen = int(len(res[0])/2)
		res_y_cen = int(len(res)/2)

		center_crop = RGBFrame [res_y_cen-int(self.H/2):res_y_cen+int(self.H/2), res_x_cen-int(self.W/2):res_x_cen+int(self.W/2)]
		center_crop_thres = eroded_frame_2 [res_y_cen-int(self.H/2):res_y_cen+int(self.H/2), res_x_cen-int(self.W/2):res_x_cen+int(self.W/2)]
		contours = cv2.findContours( eroded_frame_2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]

		x = y = w = h = 0
		areaList = []
		xList = []
		largeCoord = []
		for i, cnt in enumerate(contours):
			areaList.append(cv2.contourArea(cnt))
			#x,_,_,_ = cv2.boundingRect(cnt)
			#xList.append(x)

		# sorting the contours by area
		sortInd = sorted(range(len(areaList)), key=lambda x:areaList[x])

		#print ("area", areaList, sortInd)

		# Total num of contours
		totalNumCont = len(areaList)
		for ind in range (self.NumPills):
			if (totalNumCont-1) - ind >= 0:
				largeInd = sortInd[(totalNumCont-1) - ind]
				cnt = contours[largeInd]
				x, _, _, h = cv2.boundingRect(cnt)
				xList.append([x,h])
				largeCoord.append(largeInd)


		# sorting the contours by x coordinate
		xCoordSortInd = sorted(range(len(xList)), key=lambda x:xList[x][0])


		if len(xCoordSortInd) is self.NumPills:
			middleImageInd = xCoordSortInd[1]
			middleImageHeight = xList[middleImageInd][1]

			leftImageInd = xCoordSortInd[0]
			leftImageHeight = xList[leftImageInd][1]

			rightImageInd = xCoordSortInd[2]
			rightImageHeight = xList[rightImageInd][1]

			leftResizedBy  = middleImageHeight/leftImageHeight
			rightResizedBy = middleImageHeight/rightImageHeight


		extractedImageList = []
		#extractedImageList.append (eroded_frame_2)
		#cv2.imshow ('eroded_frame_2', eroded_frame_2)
		#if totalNumCont is not 0:
		if len(xCoordSortInd) is self.NumPills:
			for ind in range(self.NumPills):
				#print ('(totalNumCont-1) - ind', (totalNumCont-1), ind)
				if (totalNumCont-1) - ind >= 0:
					largeInd = sortInd[(totalNumCont-1) - ind]
					cnt = contours[largeInd]
					x,y,w,h = cv2.boundingRect(cnt)

					if x is 0 and y is 0 and w is 0 and h is 0:
						#return False, center_crop, center_crop_thres
						#extractedImageList.append (center_crop)
						pass
					else:
						x_center = int(x+w/2)
						y_center = int(y+h/2)

						x1, x2 = x_center-int(self.W/2), x_center+int(self.W/2)
						y1, y2 = y_center-int(self.H/2), y_center+int(self.H/2)

						if x1 < 0:
							x1 = 0
						if y1 < 0:
							y1 = 0
						if x2 >= self.initialWidth:
							x2 = self.initialWidth-1
						if y2 >= self.initialHeight:
							y2 = self.initialHeight-1
						finalImage = res [y1:y2, x1:x2]
						thresImg = eroded_frame_2 [y1:y2, x1:x2]
						if ( y2-y1 is self.H and x2-x1 is self.W ):
							if largeInd is largeCoord[xCoordSortInd[0]]:
								flipImage = cv2.flip(finalImage, 1)
								extractedImageList.append (cv2.resize(finalImage, (self.W, self.H), interpolation = cv2.INTER_CUBIC))
								resizedImage = cv2.resize(flipImage, None, fx = leftResizedBy, fy = leftResizedBy, interpolation = cv2.INTER_CUBIC)

								# crop center of image
								resizedImage = resizedImage [int(len(resizedImage)/2 - self.H/2) : int(len(resizedImage)/2 + self.H/2),
															 int(len(resizedImage[0])/2 - self.W/2) : int(len(resizedImage[0])/2 + self.W/2)]
								extractedImageList.append (cv2.resize(resizedImage, (self.W, self.H), interpolation = cv2.INTER_CUBIC))

							elif largeInd is largeCoord[xCoordSortInd[-1]]:
								flipImage = cv2.flip(finalImage, 1)
								extractedImageList.append (cv2.resize(finalImage, (self.W, self.H), interpolation = cv2.INTER_CUBIC))
								resizedImage = cv2.resize(flipImage, None, fx = rightResizedBy, fy = rightResizedBy, interpolation = cv2.INTER_CUBIC)
								# crop center of image
								resizedImage = resizedImage [int(len(resizedImage)/2 - self.H/2) : int(len(resizedImage)/2 + self.H/2),
															 int(len(resizedImage[0])/2 - self.W/2) : int(len(resizedImage[0])/2 + self.W/2)]
								extractedImageList.append (cv2.resize(resizedImage, (self.W, self.H), interpolation = cv2.INTER_CUBIC))

							else:
								extractedImageList.append (cv2.resize(finalImage, (self.W, self.H), interpolation = cv2.INTER_CUBIC))
						else:
							pass


		biggestPillInd =  self.biggestPill(extractedImageList)
		if biggestPillInd is None:
			return eroded_frame_2, extractedImageList, None
		else:
			#print ('biggestPillInd ', biggestPillInd)
			return eroded_frame_2, extractedImageList, cv2.resize(extractedImageList[biggestPillInd], (self.W, self.H), interpolation = cv2.INTER_CUBIC)
