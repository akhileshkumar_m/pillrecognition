# import the necessary packages
from .shallownet import ShallowNet
from .lenet import LeNet
from .minivggnet import MiniVGGNet
from .fine_tune_FCL import fine_tune_FCL
