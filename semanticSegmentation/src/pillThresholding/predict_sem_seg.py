# USAGE
# python rank_accuracy.py

# import the necessary packages
#from config import pill_6_config as config
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import SimplePreprocessor
from pyimagesearch.preprocessing import MeanPreprocessor
from pyimagesearch.utils.ranked import rank5_accuracy
from pyimagesearch.io import HDF5DatasetGenerator
from keras.models import load_model
import json
import cv2
import argparse
from sklearn.metrics import classification_report
import numpy as np
from keras.applications import VGG16


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, required=True,
	help="path to *specific* model checkpoint to load")
args = vars(ap.parse_args())

cam = cv2.VideoCapture(0)
if not cam.isOpened():
	print ("cam not found")
	exit()

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)
# load the pre-trained network
print("[INFO] loading model...")
model_VGG16 = VGG16(weights="imagenet", include_top=False)
model = load_model(args["model"])
ret, frame = cam.read()

height = len(frame)
width = len(frame[0])

minOfHW = min(height, width)

cv2.namedWindow ('full frame', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('full frame', int(1280/2), int(720/2))

MORPHOLOGY = 1
LARGE_IMAGE = 1

img_size = 224
def fix_img_size (image):
	rows,cols = image.shape[:2]
	max_ = max(rows, cols)
	if image.shape[-1] is 3:
		image_out = np.zeros((max_, max_, 3), dtype = "uint8")
	else:
		image_out = np.zeros((max_, max_), dtype = "uint8")
	shift_c = (max_ - cols)/2
	shift_r = (max_ - rows)/2
	shift_c = int(shift_c)
	shift_r = int(shift_r)
	image_out[shift_r : shift_r + rows, shift_c:shift_c + cols,] = image
	fx = img_size / image_out.shape[0]
	resized_image = cv2.resize(image_out, (0,0), fx = fx, fy=fx, interpolation = cv2.INTER_AREA)
	return resized_image

while True:
	ret, frame = cam.read()
	frame_copy = frame.copy()
	'''
	if LARGE_IMAGE == 1:
		frame_crop = frame[ int ( int(len(frame)/2) - minOfHW/2) : int (int(len(frame)/2)  + minOfHW/2 ) , int( int(len(frame[0])/2) - minOfHW/2 ) : int( int(len(frame[0])/2) + minOfHW/2) ]
		frame_crop = cv2.resize(frame_crop, (224, 224), interpolation = cv2.INTER_AREA)
	else:
		frame_crop = frame[ int ( int(len(frame)/2) - 224/2) : int (int(len(frame)/2)  + 224/2 ) , int( int(len(frame[0])/2) - 224/2 ) : int( int(len(frame[0])/2) + 224/2) ]
	'''
	frame_crop = fix_img_size (frame)
	frame_crop = frame_crop.astype("float")/255.0
	frame_crop_input = np.expand_dims(frame_crop, axis=0)

	features = model_VGG16.predict(frame_crop_input, batch_size=1)

	# reshape the features so that each image is represented by
	# a flattened feature vector of the `MaxPooling2D` outputs
	features = features.reshape((1, 512 * 7 * 7))

	predictions = model.predict_proba(features, batch_size=1, verbose=0)

	if predictions is not None:
#		print ('predictions : ', predictions.shape)
		predictions = predictions[0]

		if MORPHOLOGY == 1:
			ret, thresholded_frame = cv2.threshold( predictions, 1/255.0, 255, 0 )
			kernel = np.ones( ( 3, 3 ), np.uint8 )
			eroded_frame_1 = cv2.erode( thresholded_frame, kernel, iterations=3 )
			dilated_frame = cv2.dilate( eroded_frame_1, kernel, iterations=3 )
			cv2.imshow ('3 predictions', dilated_frame)

		#predictions = int(predictions)
		cv2.imshow ('1 predictions', predictions)
		#ret, predictions = cv2.threshold( predictions, .8, 255, 0 )
		#cv2.imshow ('2 predictions', predictions)

	cv2.imshow ('full frame', frame_copy)
	cv2.imshow ('crop frame', frame_crop)


	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
