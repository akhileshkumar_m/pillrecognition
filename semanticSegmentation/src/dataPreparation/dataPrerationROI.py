# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorFullImage
import sys

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

'''
cam.set (cv2.CAP_PROP_FRAME_WIDTH, 640)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 480)


'''

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

imageCount = 315
'''
for skip in range (15):
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()

'''
#bgImage = cv2.imread ('/home/prabu-test/MachineLearning/Akhil/Hackathon/dataset/pill_4/originalImages/bg.jpg')

image = cv2.imread ('Gelusil_MPS_0.bmp')
#imgX, imgY = int(image.shape[1]/2), int(image.shape[0]/2)

croppedImage = image

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessorFullImage(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)

def pastingObject (mask, extImage, bgImage):
	invertMask = (255-mask)
	cv2.imshow ('invertMask', invertMask)
	andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)
	cv2.imshow ("andResult", andResult)
	orResult = andResult + extImage
	return orResult


image = cv2.imread ('Advance_Crocin_advance_0_0.bmp')
"""
if ret is False:
	print ("image not found")
	exit()
"""

tempImage = image.copy()
'''
tempImage[imgY-int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
tempImage[imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX+int(crop_w/2)] = 255
tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2)] = 255
'''
fullImage = image.copy()
mask, extractedImage = extImage.preprocess (image)


ret, bgImage = cam.read()
imgX, imgY = int(bgImage.shape[1]/2), int(bgImage.shape[0]/2)


while True:
	ret, bgImage = cam.read()
	cv2.imshow ('Image', bgImage)
	'''
	image = cv2.imread ('/home/prabu-test/MachineLearning/Akhil/Hackathon/dataset/pill_4/originalImages/Advance_Crocin_advance_326_full.bmp')
	"""
	if ret is False:
		print ("image not found")
		exit()
	"""
	tempImage = image.copy()
	tempImage[imgY-int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2)] = 255

	fullImage = image.copy()
	mask, extractedImage = extImage.preprocess (image)
	'''
	bgImage = bgImage [imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)]
	# pasting object
	newImage = pastingObject (mask, extractedImage, bgImage)
	cv2.imshow ('new Image', newImage)
	cv2.imshow ('Cropped Image', extractedImage)
	cv2.imshow ('Full Image', tempImage)
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'n':
		if (sys.version_info > (3, 0)):
			dirName = input ("Inter directory name after {} :".format(args['output']))
		else:
			dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], 'full', str(imageCount)+"_full.bmp"]), fullImage)
			cv2.imwrite (os.path.sep.join([args['output'], 'extracted', str(imageCount)+".bmp"]), extractedImage)
			imageCount += 1
