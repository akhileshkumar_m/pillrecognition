# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter_sem_seg
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-o", "--output", required=True,
	help="path to output HDF5 file")
ap.add_argument("-b", "--batch-size", type=int, default=1,
	help="batch size of images to be passed through network")
ap.add_argument("-s", "--buffer-size", type=int, default=10,
	help="size of feature extraction buffer")

args = vars(ap.parse_args())

# store the batch size in a convenience variable
bs = args["batch_size"]

# grab the list of images that we'll be describing then randomly
# shuffle them to allow for easy training and testing splits via
# array slicing during training time
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
random.shuffle(imagePaths)
labelPaths = imagePaths.copy()
for ind in range (len (labelPaths)):
	path = labelPaths[ind].split(os.path.sep)
	path[-2] = "labels"
	labelPaths[ind] = os.path.sep.join(path)
	
#exit()

# extract the class labels from the image paths then encode the
# labels
labels = [p.split(os.path.sep)[-2] for p in imagePaths]
le = LabelEncoder()
labels = le.fit_transform(labels)

# load the VGG16 network
print("[INFO] loading network...")
model = VGG16(weights="imagenet", include_top=False)

# initialize the HDF5 dataset writer, then store the class label
# names in the dataset
dataset = HDF5DatasetWriter_sem_seg((len(imagePaths), 512 * 7 * 7), (len(imagePaths), 224, 224),
	args["output"], dataKey="features", bufSize=args["buffer_size"])
#dataset.storeClassLabels(le.classes_)

# initialize the progress bar
widgets = ["Extracting Features: ", progressbar.Percentage(), " ",
	progressbar.Bar(), " ", progressbar.ETA()]
pbar = progressbar.ProgressBar(maxval=len(imagePaths),
	widgets=widgets).start()

# loop over the images in patches
for i in np.arange(0, len(imagePaths), bs):
	# extract the batch of images and labels, then initialize the
	# list of actual images that will be passed through the network
	# for feature extraction
	batchPaths = imagePaths[i:i + bs]
	batchLabels = labelPaths[i:i + bs]
	batchImagesFeature = []
	batchLabelsFeature = []

	# loop over the images in the current batch
	for (j, imagePath) in enumerate(batchPaths):
		# load the input image using the Keras helper utility
		# while ensuring the image is resized to 224x224 pixels
		image = load_img(imagePath, target_size=(224, 224))
		image = img_to_array(image)
		image = image.astype("float")/255.0
		# preprocess the image by (1) expanding the dimensions and
		# (2) subtracting the mean RGB pixel intensity from the
		# ImageNet dataset
		image = np.expand_dims(image, axis=0)
		#image = imagenet_utils.preprocess_input(image)

		# add the image to the batch
		batchImagesFeature.append(image)
	
	# loop over the labels in the current batch
	for (j, imagePath) in enumerate(batchLabels):
		# load the input image using the Keras helper utility
		# while ensuring the image is resized to 224x224 pixels
		image = load_img(imagePath, grayscale=True, target_size=(224, 224))
		image = img_to_array(image)
		image = (image.astype("float")/255.0).reshape ((224, 224))
		
		# preprocess the image by (1) expanding the dimensions and
		# (2) subtracting the mean RGB pixel intensity from the
		# ImageNet dataset
		#image = np.expand_dims(image, axis=0)
		#image = imagenet_utils.preprocess_input(image)
		
		# add the image to the batch
		batchLabelsFeature.append(image)		
	
	# pass the images through the network and use the outputs as
	# our actual features
	batchImagesFeature = np.vstack(batchImagesFeature)
	featuresImages = model.predict(batchImagesFeature, batch_size=bs)
	'''
	batchLabelsFeature = np.vstack(batchLabelsFeature)
	featuresLabels = model.predict(batchLabelsFeature, batch_size=bs)
	'''
	# reshape the features so that each image is represented by
	# a flattened feature vector of the `MaxPooling2D` outputs
	featuresImages = featuresImages.reshape((featuresImages.shape[0], 512 * 7 * 7))
	#featuresLabels = featuresLabels.reshape((featuresLabels.shape[0], 512 * 7 * 7))
	'''
	print ('batchLabelsFeature : ', len(batchLabelsFeature))
	print ('batchLabelsFeature : ', len(batchLabelsFeature))
	print ('batchLabelsFeature : ', len(batchLabelsFeature))
	'''
	#print ('batchLabelsFeature[1] : ', np.asarray(batchLabelsFeature).shape)
	'''
	print ('batchLabelsFeature[0] : ', len(batchLabelsFeature[0]))
	print ('batchLabelsFeature[0] : ', len(batchLabelsFeature[0]))
	print ('batchLabelsFeature[0] : ', len(batchLabelsFeature[0]))
	print ('batchLabelsFeature[1] : ', len(batchLabelsFeature[1]))
	print ('batchLabelsFeature[1] : ', len(batchLabelsFeature[1]))
	'''
	#featuresLabels = batchLabelsFeature.reshape((batchLabelsFeature.shape[0], 224*224*3))
	
	# add the features and labels to our HDF5 dataset
	dataset.add(featuresImages, batchLabelsFeature)
	pbar.update(i)

# close the dataset
dataset.close()
pbar.finish()
