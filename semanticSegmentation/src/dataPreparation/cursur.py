# import the necessary packages
import argparse
import cv2
import numpy as np

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False
margin = 40
def click_and_crop(event, x, y, flags, param):
	print ('x : y : ',x, y, hsv[y, x])
	(H, S, V) = hsv[y, x]
#    lower_blue = np.array([H-margin, S-margin, V-margin])
#    upper_blue = np.array([H+margin, S+margin, V+margin])

	lower_blue = np.array([H-margin, S-margin, V-255])
	upper_blue = np.array([H+margin, S+margin, V+255])
    #lower_blue = np.array([H-margin, S-margin, 0])
    #upper_blue = np.array([H+margin, S+margin, 255])


    # Threshold the HSV image to get only blue colors
	mask = cv2.inRange(hsv, lower_blue, upper_blue)
	# Bitwise-AND mask and original image
	res = cv2.bitwise_and(image,image, mask= mask)
	cv2.imshow('res',res)


	# grab references to the global variables
	global refPt, cropping

	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

		# draw a rectangle around the region of interest
		cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
		cv2.imshow("image", image)

# construct the argument parser and parse the arguments
'''
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
'''
# load the image, clone it, and setup the mouse callback function
#image = cv2.imread(args["image"])
#hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

cap = cv2.VideoCapture(0)
status, image = cap.read()
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
clone = image.copy()
cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)

# keep looping until the 'q' key is pressed
while True:
	# display the image and wait for a keypress
	status, image = cap.read()
	if status is None:
		break

	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
	cv2.imshow("image", image)

	key = cv2.waitKey(1) & 0xFF

	# if the 'r' key is pressed, reset the cropping region
	if key == ord("r"):
		image = clone.copy()

	# if the 'c' key is pressed, break from the loop
	elif key == ord("c"):
		break

# if there are two reference points, then crop the region of interest
# from teh image and display it
if len(refPt) == 2:
	roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
	cv2.imshow("ROI", roi)
	cv2.waitKey(0)

# close all open windows
cv2.destroyAllWindows()
