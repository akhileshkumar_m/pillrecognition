# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImages
import sys

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

imageCount = 0

for skip in range (15):
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
imgX, imgY = int(image.shape[1]/2), int(image.shape[0]/2)

croppedImage = image

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
#extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
extImage = ExtractPreprocessorTwoImages(GRAYBaseFrame=first_gray, thresholdValue=100, H=crop_h, W=crop_w)
while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
	tempImage = image.copy()
	tempImage[imgY-int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2)] = 255

	fullImage = image.copy()
	status, extractedImage, extractedImageThres = extImage.preprocess (image)

	cv2.imshow ('CextractedImageThres', extractedImageThres)
	cv2.imshow ('extractedImage', extractedImage)
	cv2.imshow ('Full Image', tempImage)
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'n':
		if (sys.version_info > (3, 0)):
			dirName = input ("Inter directory name after {} :".format(args['output']))
		else:
			dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			#cv2.imwrite (os.path.sep.join([args['output'], 'full', dirName, dirName+'_'+str(imageCount)+"_full.bmp"]), fullImage)
			#cv2.imwrite (os.path.sep.join([args['output'], 'extracted', dirName, dirName+'_'+str(imageCount)+".bmp"]), extractedImage)
			cv2.imwrite (os.path.sep.join([args['output'], dirName+'_'+str(imageCount)+".bmp"]), extractedImage)
			imageCount += 1
