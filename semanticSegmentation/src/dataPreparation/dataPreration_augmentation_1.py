# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorFullImage
import sys
import random
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-d", "--dataset", default=None,
	help="path to model")
ap.add_argument("-b", "--baseImage", default=None,
	help="path to model")
ap.add_argument("-p", "--pills", default=None,
	help="path to pills dataset")

args = vars(ap.parse_args())
crop_w = 224
crop_h = 224
MARGIN = 50

imagePaths = list(paths.list_images(args["dataset"]))
random.shuffle(imagePaths)

refImage = cv2.imread (args["baseImage"])
refImage = cv2.cvtColor(refImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessorFullImage(GRAYBaseFrame=refImage, thresholdValue=10, H=crop_h, W=crop_w)

imgXList = [200, 500, 800, 1000]
imgYList = [200, 350, 500]
img_size = 224


def fix_img_size (image):
	rows,cols = image.shape[:2]
	max_ = max(rows, cols)
	if image.shape[-1] is 3:
		image_out = np.zeros((max_, max_, 3), dtype = "uint8")
	else:
		image_out = np.zeros((max_, max_), dtype = "uint8")
	shift_c = (max_ - cols)/2
	shift_r = (max_ - rows)/2
	shift_c = int(shift_c)
	shift_r = int(shift_r)
	image_out[shift_r : shift_r + rows, shift_c:shift_c + cols,] = image
	fx = img_size / image_out.shape[0]
	resized_image = cv2.resize(image_out, (0,0), fx = fx, fy=fx, interpolation = cv2.INTER_AREA)
	return resized_image

def pastingObject (mask, extImage, bgImage):
	invertMask = (255-mask)
	'''
	cv2.imshow ('invertMask', invertMask)
	cv2.imshow ('bgImage', bgImage)
	cv2.waitKey(0)
	'''
	andResult = cv2.bitwise_and(bgImage, bgImage, mask = invertMask)
	#cv2.imshow ("andResult", andResult)
	orResult = andResult + extImage
	return orResult

imageCount = 0



pillImagePaths = list(paths.list_images(args["pills"]))

for PIP in pillImagePaths:
	objImage = cv2.imread (PIP)
	fullImage = objImage.copy()
	mask, extractedImage = extImage.preprocess (objImage)
	objFileName = (PIP.split(os.path.sep)[-1]).split(".")[0]

	for im in imagePaths:
		bgImage = cv2.imread (im)
		bgName = (im.split(os.path.sep)[-1]).split(".")[0]
		for imgX in imgXList:
			for imgY in imgYList:
				tempImage = bgImage.copy()
				#cv2.imshow ('tempImage', tempImage)
				croppedImage = bgImage.copy()
				labelImage = np.zeros(tempImage.shape[:2],dtype=np.uint8)

				croppedImage = croppedImage [imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)]
				newImage = pastingObject (mask, extractedImage, croppedImage)
				tempImage [imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = newImage
				labelImage [imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = mask

				tempImage_1  = fix_img_size(tempImage)
				labelImage_1 = fix_img_size(labelImage)

				cv2.imwrite (os.path.sep.join([args['output'], 'train', bgName+"_"+objFileName+"_"+str(imgY)+"_"+str(imgX)+".bmp"]), tempImage_1)
				cv2.imwrite (os.path.sep.join([args['output'], 'labels', bgName+"_"+objFileName+"_"+str(imgY)+"_"+str(imgX)+".bmp"]), labelImage_1)
				imageCount += 1
