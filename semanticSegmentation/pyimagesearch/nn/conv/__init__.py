# import the necessary packages
from .shallownet import ShallowNet
from .lenet import LeNet
from .minivggnet import MiniVGGNet
from .fcheadnet import FCHeadNet
from .fine_tune_FCL import fine_tune_FCL
from .fine_tune_FCL_2 import fine_tune_FCL_2
from .fine_tune_3_FCL import fine_tune_3_FCL
from .fine_tune_FCL_DO import fine_tune_FCL_DO
from .fine_tune_without_DO import fine_tune_without_DO
from .fine_tune_3_FCL_DO import fine_tune_3_FCL_DO
from .fine_tune_FCL_act import fine_tune_FCL_act
from .fine_tune_1_FCL import fine_tune_1_FCL
from .SEM_SEG_VGG16 import SEM_SEG_VGG16_NET
from .SEM_SEG_VGG16_1 import SEM_SEG_VGG16_NET_1
from .SEM_SEG_VGG16_3 import SEM_SEG_VGG16_NET_3
from .SEM_SEG_VGG16_4 import SEM_SEG_VGG16_NET_4
from .SEM_SEG_VGG16_5 import SEM_SEG_VGG16_NET_5
from .SEM_SEG_VGG16_6 import SEM_SEG_VGG16_NET_6
