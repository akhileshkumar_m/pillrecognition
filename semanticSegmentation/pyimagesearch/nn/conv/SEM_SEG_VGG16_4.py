# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras import backend as K
from keras.layers import Conv2DTranspose
from keras.layers import Reshape
from keras.backend import shape
from keras.layers import UpSampling2D
from keras.layers.core import Dropout

class SEM_SEG_VGG16_NET_4:
	@staticmethod
	def build(width, height, depth):
		# initialize the model along with the input shape to be
		# "channels last"
		model = Sequential()
		inputShape = (height, width, depth)
		DO = 0.5
		model.add(Reshape ((7, 7, -1), input_shape=(25088,)))
		print ("shape : ", model.output.shape)

		model.add(UpSampling2D(size=(2, 2), data_format=None))
		print ("shape : ", model.output.shape)

		model.add(Conv2DTranspose(256, 3, strides=(1, 1), padding='same', activation='relu'))
		model.add(Dropout(DO))
		model.add(Conv2DTranspose(64, 3, strides=(1, 1), padding='same', activation='relu'))
		model.add(UpSampling2D(size=(2, 2), data_format=None))
		model.add(UpSampling2D(size=(2, 2), data_format=None))
		model.add(Dropout(DO))
		model.add(Conv2DTranspose(32, 3, strides=(1, 1), padding='same', activation='relu'))
		model.add(UpSampling2D(size=(2, 2), data_format=None))
		model.add(UpSampling2D(size=(2, 2), data_format=None))
		model.add(Dropout(DO))
		model.add(Conv2DTranspose(1, 3, strides=(1, 1), padding='same', activation='relu'))
		model.add(Reshape ((224,224)))

		# return the constructed network architecture
		return model
