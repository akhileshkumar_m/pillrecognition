# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
import glob

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-i", "--input", required=True,
	help="path of input images")	
ap.add_argument("-e", "--ext", required=True,
	help="extension of input images (full.bmp is acceptable)")	
ap.add_argument("-o", "--output", default=None,
	help="path to output of the images")	
args = vars(ap.parse_args())

# load the VGG16 network
print("[INFO] loading network...")
model = VGG16(weights="imagenet", include_top=False)

cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')
#	exit()

#for i in np.arange(0, len(imagePaths), bs):

f = open(args["model"], "rb")
model_1 = pickle.load(f)

fileName = args["model"].split(".")
fileName [-2] = fileName [-2]+"_labels"
fileName = ".".join(fileName)

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
#print ("len of score : ", len(score))
imageCount = 0
'''
while True:
	ret, image = cam.read()
'''
inputPath = args["input"]+"*"+args["ext"]
for img in glob.glob(inputPath):
	image = cv2.imread(img)
#	if ret is False:
#		print ("image not found")
#		exit()
	tempImage = image.copy()
	fullImage = image.copy()
	imgX, imgY = image.shape[1]/2, image.shape[0]/2
	image = image [imgY-112:imgY+112, imgX-112:imgX+112]
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)
	features = model.predict(image, batch_size=1)

	# reshape the features so that each image is represented by
	# a flattened feature vector of the `MaxPooling2D` outputs
	features = features.reshape((1, 512 * 7 * 7))
	preds = model_1.predict_proba(features)
#	print ('preds : ', preds.shape)

#	preds = model_1.score(features, [2])

#	print ('preds : ', np.around(preds*100, 1))

	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
#		print (cl)
		val = preds[0][cl]*100
#		print ("  ", val)
		score[cl] = round(val, 2)
	best_score = score[int(pred)]
#	print ('	preds : ', score)

#	print(classification_report(preds, preds))
	for cl in range(len(labels)):
		cv2.putText(tempImage, labels[cl]+' '+str(score[cl]), (30, 50 + 30*cl), 0, 0.8, (0, 255, 0), 2, cv2.LINE_AA)

	cv2.putText(tempImage, labels[int(pred)]+' '+str(best_score), (300, 50), 0, 0.8, (0, 0, 0), 2, cv2.LINE_AA)


	tempImage[imgY-112, imgX-112:imgX+112] = 0
	tempImage[imgY+112, imgX-112:imgX+112] = 0
	tempImage[imgY-112:imgY+112, imgX+112] = 0
	tempImage[imgY-112:imgY+112, imgX-112] = 0
	
	cv2.imshow ('frame', tempImage)
	k = cv2.waitKey(0)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (args['output']+ args['output'].split(os.path.sep)[-1] + str(imageCount)+".bmp", tempImage)
			cv2.imwrite (args['output']+ args['output'].split(os.path.sep)[-1] + str(imageCount)+"_full.bmp", fullImage)
			cv2.imwrite (args['output']+ args['output'].split(os.path.sep)[-1] + str(imageCount)+"_cropped.bmp", croppedImage)
			imageCount += 1
	
f.close()
