# USAGE
# python train_model.py --db ../datasets/animals/hdf5/features.hdf5 \
#	--model animals.cpickle
# python train_model.py --db ../datasets/caltech101/hdf5/features.hdf5 \
#	--model caltech101.cpickle
# python train_model.py --db ../datasets/flowers17/hdf5/features.hdf5 \
#	--model flowers17.cpickle

# import the necessary packages
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
import numpy as np
import datetime


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--db", required=True,
	help="path HDF5 database")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
ap.add_argument("-j", "--jobs", type=int, default=-1,
	help="# of jobs to run when tuning hyperparameters")
args = vars(ap.parse_args())

# open the HDF5 database for reading then determine the index of
# the training and testing split, provided that this data was
# already shuffled *prior* to writing it to disk
db = h5py.File(args["db"], "r")
i = int(db["labels"].shape[0] * 0.9)


# define the set of parameters that we want to tune then start a
# grid search where we evaluate our model for each value of C
print("[INFO] tuning hyperparameters...")
#params = {"C": [0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0]}
params = {"C": [0.1]}
model = GridSearchCV(LogisticRegression(), params, cv=3,
	n_jobs=args["jobs"])

#print ('unique                      : ', np.unique(db["labels"][:i]))
print ('Shape of training dataset   : {}'.format(db["features"][:i].shape))
print ('Shape of validation dataset : {}'.format(db["features"][i:].shape))
print ('Classes                     : ', db["label_names"][:])
start_time = datetime.datetime.now()
print ('Start time                                             {}'.format(start_time.strftime('%Y-%m-%d %H:%M:%S')))

model.fit(db["features"][:i], db["labels"][:i])
print("[INFO] best hyperparameters  : {}".format(model.best_params_))

# serialize the model to disk
print("[INFO] saving model...")
f = open(args["model"], "wb")
f.write(pickle.dumps(model.best_estimator_))
f.close()


fileName = args["model"].split(".")
fileName [-2] = fileName [-2]+"_labels"
fileName = ".".join(fileName)
#print ("fileName : ", fileName)
#print ('db["label_names"] : ', db["label_names"][:])

f = open(fileName, "wb")
f.write(pickle.dumps(db["label_names"][:]))
f.close()


time_now_1 = datetime.datetime.now()
print ('Time after training and serializing model              {}'.format(time_now_1.strftime('%Y-%m-%d %H:%M:%S')))
time_difference_1 = time_now_1 - start_time
print ('Time elpased in training and serializing (hh:mm:ss.ms) {}'.format(time_difference_1))


# evaluate the model
print("[INFO] evaluating...")
preds = model.predict(db["features"][i:])
print(classification_report(db["labels"][i:], preds,
	target_names=db["label_names"]))

time_now_2 = datetime.datetime.now()
print ('Time after training and serializing model              {}'.format(time_now_2.strftime('%Y-%m-%d %H:%M:%S')))
time_difference_2 = time_now_2 - time_now_1
print ('Time elpased in evaluation of model      (hh:mm:ss.ms) {}'.format(time_difference_2))

# close the database
db.close()
