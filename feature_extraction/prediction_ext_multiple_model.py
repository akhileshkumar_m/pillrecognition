# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
from pyimagesearch.preprocessing import ExtractPreprocessor

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-s", "--SLM", required=True, #second layer model
	help="path to model")
args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50

# load the VGG16 network
print("[INFO] loading network...")
model = VGG16(weights="imagenet", include_top=False)
cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

f = open(args["model"], "rb")

model_1 = pickle.load(f)

fileName = args["model"].split(".")
fileName [-2] = fileName [-2]+"_labels"
fileName = ".".join(fileName)

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
secondLayerModelLabels = [None]*(len(labels))
secondLayerModel = [None]*(len(labels))

for model_index in range(len(labels)):
	model_path = os.path.sep.join([args['SLM'], labels[model_index], labels[model_index]+'.cpickle'])
	if os.path.exists(model_path):
		secondLayerModel[model_index] = pickle.load(open(model_path, "rb"))
		fileName = model_path.split(".")
		fileName [-2] = fileName [-2]+"_labels"
		fileName = ".".join(fileName)
		secondLayerModelLabels[model_index] = pickle.load(open(fileName, "rb"))

imageCount = 0

for skip in range (15):
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
imgX, imgY = image.shape[1]/2, image.shape[0]/2

#croppedImage = image [imgY-crop_h/2-MARGIN:imgY+crop_h/2+MARGIN, imgX-crop_w/2-MARGIN:imgX+crop_w/2+MARGIN]
croppedImage = image

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)

while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
	tempImage = image.copy()
	tempImage[imgY-crop_h/2, imgX-crop_w/2:imgX+crop_w/2] = 0
	tempImage[imgY+crop_h/2, imgX-crop_w/2:imgX+crop_w/2] = 0
	tempImage[imgY-crop_h/2:imgY+crop_h/2, imgX+crop_w/2] = 0
	tempImage[imgY-crop_h/2:imgY+crop_h/2, imgX-crop_w/2] = 0

	fullImage = image.copy()

	#image = image [imgY-crop_h/2-MARGIN:imgY+crop_h/2+MARGIN, imgX-crop_w/2-MARGIN:imgX+crop_w/2+MARGIN]

	status, extractedImage = extImage.preprocess (image)
	cv2.imshow("extractedImage", extractedImage)
	#print ('status : {}'.format(status))
	#if extractedImage is F:
	#extractedImage = tempImage[imgY-crop_h/2:imgY+crop_h/2, imgX-crop_w/2:imgX+crop_w/2]

	image = extractedImage.copy()
	canvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
	secondCanvas = np.zeros((35*len(labels)+5, 300, 3), dtype="uint8")
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)
	features = model.predict(image, batch_size=1)

	# reshape the features so that each image is represented by
	# a flattened feature vector of the `MaxPooling2D` outputs
	features = features.reshape((1, 512 * 7 * 7))
	preds = model_1.predict_proba(features)
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		w = int (score[cl]*3 )
		cv2.rectangle(canvas, (5, (cl * 35) + 5), (w+5, (cl * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10, (cl * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	cv2.putText(tempImage, 'First Layer', (50, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
	cv2.putText(tempImage, ': '+labels[int(pred)]+' '+str(best_score), (250, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
	# Second layer model starting............
	secondModelIndex = int(pred)
	if secondLayerModel[secondModelIndex] is not None:
		preds = secondLayerModel[secondModelIndex].predict_proba(features)
		pred = np.argmax(preds, axis=1)
		for cl in range(len(secondLayerModelLabels[secondModelIndex])):
			val = preds[0][cl]*100
			score[cl] = round(val, 2)
		best_score = score[int(pred)]

		for cl in range(len(secondLayerModelLabels[secondModelIndex])):
			w = int (score[cl]*3 )
			cv2.rectangle(secondCanvas, (5, (cl * 35) + 5), (w+5, (cl * 35) + 35), (0, 0, 255), -1)
			cv2.putText(secondCanvas, secondLayerModelLabels[secondModelIndex][int(cl)]+' '+str(score[cl]), (10, (cl * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

		cv2.putText(tempImage, 'Second Layer', (50, 80), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
		cv2.putText(tempImage, ': '+secondLayerModelLabels[secondModelIndex][int(pred)]+' '+str(best_score), (250, 80), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)
		#second layer model ended................
	cv2.imshow ('croppedImage', croppedImage)
	cv2.imshow ('probabilities', canvas)
	cv2.imshow ('secondLayerProbabilities', secondCanvas)
	cv2.imshow ('frame', tempImage)

	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'n':
		dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], dirName, dirName+'_'+str(imageCount)+"_full.bmp"]),        fullImage)
			cv2.imwrite (os.path.sep.join([args['output'], dirName, dirName+'_'+str(imageCount)+".bmp"]),             tempImage)
			cv2.imwrite (os.path.sep.join([args['output'], dirName, dirName+'_'+str(imageCount)+"_cropped.bmp"]),     croppedImage)
			cv2.imwrite (os.path.sep.join([args['output'], dirName, dirName+'_'+str(imageCount)+"_probability.bmp"]), canvas)
			imageCount += 1

f.close()
