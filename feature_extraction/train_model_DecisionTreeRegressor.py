# USAGE
# python train_model.py --db ../datasets/animals/hdf5/features.hdf5 \
#	--model animals.cpickle
# python train_model.py --db ../datasets/caltech101/hdf5/features.hdf5 \
#	--model caltech101.cpickle
# python train_model.py --db ../datasets/flowers17/hdf5/features.hdf5 \
#	--model flowers17.cpickle

# import the necessary packages
import matplotlib
matplotlib.use("Agg")
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
import numpy as np
from sklearn.svm import SVC
import matplotlib.pyplot as plt



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--db", required=True,
	help="path HDF5 database")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
ap.add_argument("-j", "--jobs", type=int, default=-1,
	help="# of jobs to run when tuning hyperparameters")
args = vars(ap.parse_args())

# open the HDF5 database for reading then determine the index of
# the training and testing split, provided that this data was
# already shuffled *prior* to writing it to disk
db = h5py.File(args["db"], "r")
i = int(db["labels"].shape[0] * 0.75)

# define the set of parameters that we want to tune then start a
# grid search where we evaluate our model for each value of C
print("[INFO] tuning hyperparameters...")
params = {"C": [0.1]}

parameters = {}

#model = GridSearchCV( LinearDiscriminantAnalysis(n_components=2), cv=3)
#model = DecisionTreeRegressor()
model = GridSearchCV(DecisionTreeRegressor(), parameters, cv=3)
print ('unique : ', np.unique(db["labels"][:i]))

H = model.fit(db["features"][:i], db["labels"][:i])
#print("[INFO] best hyperparameters: {}".format(model.best_params_))

# evaluate the model
print("[INFO] evaluating...")
print ('db["features"][i:] : ', db["features"][i:].shape)

print ("[INFO] validation set...")
preds = model.predict(db["features"][i:])
print(classification_report(db["labels"][i:], preds,
	target_names=db["label_names"]))

print ('model.max_features_ : ', model.best_score_ )
print ('model.n_features_   : ', model.best_params_ )
print ('model.n_outputs_    : ', model.grid_scores_ )
print ('model.tree_         : ', model.best_estimator_ )
print ('model.n_outputs_    : ', model.scorer_ )

# serialize the model to disk
print("[INFO] saving model...")
f = open(args["model"], "wb")
#f.write(pickle.dumps(model.best_estimator_))
f.write(pickle.dumps(model))
f.close()


fileName = args["model"].split(".")
fileName [-2] = fileName [-2]+"_labels"
fileName = ".".join(fileName)
print ("fileName : ", fileName)
print ('db["label_names"] : ', db["label_names"][:])

f = open(fileName, "wb")
f.write(pickle.dumps(db["label_names"][:]))
f.close()

#p = [args["output"], "model_{}.png".format(i)]

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 40), H.history["loss"],
label="train_loss")
plt.plot(np.arange(0, 40), H.history["val_loss"],
label="val_loss")
plt.plot(np.arange(0, 40), H.history["acc"],
label="train_acc")
plt.plot(np.arange(0, 40), H.history["val_acc"],
label="val_acc")
plt.title("Training Loss and Accuracy for model {}".format(i))
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(os.path.sep.join(p))
plt.close()

# close the database
db.close()
