This project is for pill recognition.
Directory structure:
- data_augmentation : consisting code related to data augmentation
- feature_extraction : consisting information about feature extraction
- fine_tuning : consisting fine tuning pre-trained model code for static pill recognition. This directory consists of main pill recognition code. For more, you can read readme in fine_tuning directory.
- multipleView : consisting code for recognizing pill with view point.
- negativeImageGenerator : consisting code to generate negative images
- semanticSegmentation : consisting code for semantic segmentation.

For existing static pill recognition code, find the readme in fine_tuning directory