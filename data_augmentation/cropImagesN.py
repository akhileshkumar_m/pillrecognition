# USAGE
# python minivggnet_flowers17_data_aug.py --dataset ../datasets/flowers17/images --output ../dataset/flower17/training_set

# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.preprocessing import cropImages
from pyimagesearch.preprocessing import resizeImagesByN
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import MiniVGGNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-o", "--output", required=True,
	help="path to input dataset")
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]

#x_list = [10, 30, 50, 70, 90]
#y_list = [10, 30, 50, 70, 90]

x_list = [250, 260, 270]
y_list = [170, 180, 190]

#ri = resizeImagesByN (20)

totalImages = len(imagePaths)

numberOfImage = 0

for start in range(0, totalImages, 1000):
	if (totalImages - start) < 1000:
		end = totalImages
	else:
		end = start + 1000
	print ("\nstart : %d\tend : %d"%(start, end))
	iap = ImageToArrayPreprocessor()
	for ind_x in range(len(x_list)):
		for ind_y in range(len(y_list)):

			ci = cropImages(x_list[ind_x], y_list[ind_y], 224, 224)


			# load the dataset from disk then scale the raw pixel intensities
			# to the range [0, 1]

			# initialize the image preprocessors
			sdl = SimpleDatasetLoader(preprocessors=[ci, iap])
			#sdl = SimpleDatasetLoader(preprocessors=[ri])
			(data, labels) = sdl.load(imagePaths[start:end], verbose=500)
	
			(trainX, trainY) = data, labels
			print ('trainX : ', trainX.shape)

			for ind in range(len(trainX)):
				numberOfImage += 1
				#cv2.imwrite ( os.path.sep.join([args["output"], trainY[ind]+str(numberOfImage)+".bmp"]), trainX[ind])
				cv2.imwrite ( os.path.sep.join([args["output"], trainY[ind], trainY[ind] + str(numberOfImage)+".bmp"]), trainX[ind])


			print ("numberOfImage : '", numberOfImage)


"""

# convert the labels from integers to vectors
#trainY = LabelBinarizer().fit_transform(trainY)

# construct the image generator for data augmentation
aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1,
	height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
	horizontal_flip=True, fill_mode="nearest")

numberOfImage = 0

for x_train_augmented, y_train_augmented in aug.flow(trainX, trainY, batch_size=len(trainX)):
	(ht, wt, dp) = x_train_augmented[0].shape
	for ind in range(len(x_train_augmented)):
	
		numberOfImage += 1
		cv2.imwrite ( os.path.sep.join([args["training"], y_train_augmented[ind], str(numberOfImage)+".bmp"]), x_train_augmented[ind])
		if numberOfImage == 17*60*8:
			break
	if numberOfImage == 17*60*8:
			break
			

for ind in range(len(testX)):
	cv2.imwrite ( os.path.sep.join([args["validation"], testY[ind], str(numberOfImage)+".bmp"]), testX[ind])
	numberOfImage += 1
"""
