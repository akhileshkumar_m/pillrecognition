# USAGE
#python data_aug_single_dir.py --training ../../dataset/pill_6/training_flip_aug/ --dataset ../../dataset/pill_6/training_cropped/ADVANCE/ 
# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-t", "--output", required=True,
	help="path to output dataset")	
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]



for prefix in classNames:
	filePath = os.path.sep.join([args["dataset"], prefix])
	filePaths = list(paths.list_images(filePath))
	print ('prefix : ', prefix)
	
	outputPath = os.path.sep.join([args["output"], prefix])
	imageCount = 0
#	print ("filePath : ", filePaths)
	for fp in filePaths:
#		print (fp)
		image = cv2.imread (fp)
		cv2.imwrite (os.path.sep.join([outputPath, str(imageCount)+"_"+prefix+".bmp"]), image)
		imageCount += 1
