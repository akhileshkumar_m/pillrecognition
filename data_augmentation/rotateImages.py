# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2
import os
from imutils import paths

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,
	help="path to the image file")
ap.add_argument("-o", "--output", required=True,
	help="path to save the rotate images")
args = vars(ap.parse_args())

# load the image from disk
imagePaths = list(paths.list_images(args["dataset"]))

numberOfImages = len(imagePaths)
#finalNumberOfImage = 350
finalNumberOfImage = 100


angleDiff = int(int(360*numberOfImages)/finalNumberOfImage)
print ('angleDiff : {}'.format(angleDiff))
imageCount = 0
for im in imagePaths:
	image = cv2.imread(im)

	# loop over the rotation angles

	#for angle in np.arange(0, 360, 5):
	for angle in np.arange(0, 360, angleDiff):
		rotated = imutils.rotate(image, angle)
		imageName = im.split(os.path.sep)[-2]
		cv2.imwrite(os.path.sep.join([args["output"], imageName, imageName+'_'+str(imageCount) + "_" + str(angle) + ".bmp"]), rotated)
		#cv2.imwrite (os.path.sep.join([args["output"], imageName, imageName+"_"+str(imageCount)+"_"+str(angle)+".bmp"]), rotated)

		imageCount += 1
		if imageCount == finalNumberOfImage:
			break
	if imageCount == finalNumberOfImage:
		break

print ('imageCount : {}'.format(imageCount))
