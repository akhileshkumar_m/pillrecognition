# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImages
import sys
# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImages as EP
import sys
import imutils
from imutils import paths
import numpy as np
import progressbar
import random
import os
import cv2
import pickle
import h5py
from keras.models import load_model
import datetime


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to output")

args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
else:
	print ('camera opened')

cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

cv2.namedWindow ('Full Image', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('Full Image', int(1280/2), int(720/2))



imageCount = 0
thresVal = 10
def takeBackground ():
	while True:
		ret, image = cam.read()
		if ret is False:
			print ("image not found")
			exit()
		image = imutils.rotate(image, 180)
		cv2.imshow ('Full Image', image)
		k = cv2.waitKey(1)
		if chr(k&255) is 'q':
			exit()
		elif chr(k&255) is 'b':
			return image

image = takeBackground()



reqImgW = int(len(image[0]))
reqImgH = int(len(image)/2)+30

cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))

imgX, imgY = int(image.shape[1]/2), int(image.shape[0]/2)

croppedImage = image [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
#extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)
extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)

while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()

	image = imutils.rotate(image, 180)
	tempImage = image.copy()

	tempImage[imgY-int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImage[imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)] = 255
	tempImage[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX+int(reqImgW/2)-1] = 255
	tempImage[imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2)] = 255

	fullImage = image.copy()
	image = image [imgY-int(reqImgH/2):imgY+int(reqImgH/2), imgX-int(reqImgW/2):imgX+int(reqImgW/2)]
	nextBgImage = image.copy()
	status, extractedPill, extractedImage = extImage.preprocess (image)
	totalNumImg = len(extractedPill)

	cv2.imshow ('threshold Image', extractedPill)
	cv2.imshow ('Full Image', tempImage)

	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	elif chr(k&255) is 'b':
		first_gray = cv2.cvtColor(nextBgImage, cv2.COLOR_BGR2GRAY)
		extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)
		cv2.destroyAllWindows()
		cv2.namedWindow ('Full Image', cv2.WINDOW_NORMAL)
		cv2.resizeWindow ('Full Image', int(1280/2), int(720/2))
		cv2.namedWindow ('threshold Image', cv2.WINDOW_NORMAL)
		cv2.resizeWindow ('threshold Image', int(reqImgW/2), int(reqImgH/2))
	elif chr(k&255) is 'v':
		#first_gray = cv2.cvtColor(nextBgImage, cv2.COLOR_BGR2GRAY)
		thresVal = int(input("Enter threshold : "))
		extImage = EP(GRAYBaseFrame=first_gray, thresholdValue=thresVal, H=crop_h, W=crop_w)
	elif chr(k&255) is 'n':
		if (sys.version_info > (3, 0)):
			dirName = input ("Inter directory name after {} :".format(args['output']))
		else:
			dirName = raw_input ("Inter directory name after {} :".format(args['output']))
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], 'full', dirName, dirName+'_'+str(imageCount)+"_full.bmp"]), fullImage)
			cv2.imwrite (os.path.sep.join([args['output'], 'extracted', dirName, dirName+'_'+str(imageCount)+".bmp"]), extractedPill)
			imageCount += 1
