# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import os
import cv2
import argparse
from pyimagesearch.preprocessing import ExtractPreprocessorTwoImages
import sys

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-o", "--output", default=None,
	help="path to model")
ap.add_argument("-b", "--baseImage", required=True,
	help="path to input dataset")
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50
imageCount = 0

image = cv2.imread (args["baseImage"])
imgX, imgY = int(image.shape[1]/2), int(image.shape[0]/2)
croppedImage = image
first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessorTwoImages(GRAYBaseFrame=first_gray, thresholdValue=260, H=crop_h, W=crop_w)

imagePaths = list(paths.list_images(args["dataset"]))

for ip in imagePaths:
	image = cv2.imread (ip)

	tempImage = image.copy()
	'''
	tempImage[imgY-int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY+int(crop_h/2), imgX-int(crop_w/2): imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX+int(crop_w/2)] = 255
	tempImage[imgY-int(crop_h/2): imgY+int(crop_h/2), imgX-int(crop_w/2)] = 255
	'''
	fullImage = image.copy()
	status, extractedImage, extractedImageThres = extImage.preprocess (image)

	fileName = ip.split(os.path.sep)
	cv2.imshow("frame", extractedImageThres)
	#cv2.waitKey(0)
	#print ("fileName", fileName)
	cv2.imwrite (os.path.sep.join([args['output'], fileName[-1]]), extractedImageThres)
	#cv2.imwrite (os.path.sep.join([args['output'], fileName]), extractedImage)
	imageCount += 1
