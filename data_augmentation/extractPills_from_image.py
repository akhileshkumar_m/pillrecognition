import numpy as np
import cv2
import argparse
import os
import cv2
from imutils import paths

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-b", "--baseImage", required=True,
	help="path to input dataset")
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-t", "--output", required=True,
	help="path to input dataset")
args = vars(ap.parse_args())

imagePaths = list(paths.list_images(args["dataset"]))
frame = cv2.imread (args["baseImage"])

W = 150
H = 150

final_h = 224
final_w = 224

imgX, imgY = frame.shape[1]/2, frame.shape[0]/2
cropped_frame = frame [imgY-H:imgY+H, imgX-W:imgX+W]	

x_shift = imgX-W
y_shift = imgY-H

first_gray = cv2.cvtColor(cropped_frame, cv2.COLOR_BGR2GRAY)
num_capsule = 0
for ip in imagePaths:
	largest_area = 0
	#ret, frame = cap.read()
	frame = cv2.imread (ip)
	
	cropped_frame_RGB = frame [imgY-H:imgY+H, imgX-W:imgX+W]
	
	frame_copy = np.zeros(frame.shape)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (5,5), 0)
	cropped_frame = gray [imgY-H:imgY+H, imgX-W:imgX+W]
#	cv2.imshow ("cropped_frame_1", cropped_frame)
	subImg = cv2.subtract( cropped_frame, first_gray)
#	cv2.imshow ("subImg", subImg)
	ret, thresholded_frame = cv2.threshold( subImg, 30, 255, 0 )
	
	
	kernel = np.ones( ( 3, 3 ), np.uint8 )
	eroded_frame_1 = cv2.erode( thresholded_frame, kernel, iterations=3 )
	dilated_frame = cv2.dilate( eroded_frame_1, kernel, iterations=3 )
	eroded_frame_2 = dilated_frame.copy() 
	
	res = cv2.bitwise_and(cropped_frame_RGB, cropped_frame_RGB, mask = eroded_frame_2)
	cv2.imshow ("res", res)
#	cv2.imshow ("cropped_frame", cropped_frame)
	contours = cv2.findContours( eroded_frame_2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE )[1]
	
	x = y = w = h = 0
	for i, cnt in enumerate(contours):
		area = cv2.contourArea(cnt)
		if( area > largest_area):			
			x,y,w,h = cv2.boundingRect(cnt)
			largest_area = area;
			largest_contour = cnt
			largest_contour_index=i;
	if x is 0 and y is 0 and w is 0 and h is 0:
		pass
	else:
		#mask = np.zeros(img.shape, np.uint8)
		x_center = x+w/2
		y_center = y+h/2
	
		x1, x2 = x_center-final_w/2, x_center+final_w/2
		y1, y2 = y_center-final_h/2, y_center+final_h/2
		
		if x1 < 0:
			x1 = 0
		if y1 < 0:
			y1 = 0
		if x2 >= 2*W:
			x2 = 2*W-1
		if y2 >= 2*H:
			y2 = 2*H-1
		finalImage = res[y1:y2, x1:x2]
		if ( y2-y1 is final_h and x2-x1 is final_w ):
			imageName = ip.split(os.path.sep)
			cv2.imwrite (os.path.sep.join([args["output"], imageName[-2], imageName[-1]]), finalImage)
			num_capsule += 1
			if num_capsule%500 is 0:
				print ("INFO : %d"%num_capsule)
		#cv2.imshow ("finalImage", finalImage)
		'''
		largest_contour += [x_shift,y_shift]
		cv2.drawContours(frame_copy, largest_contour, -1, (0, 255, 0), cv2.FILLED)
		cv2.rectangle(frame, (x+x_shift,y+y_shift), (x+x_shift+w, y+y_shift+h), (0,255,0))
		cv2.putText(frame, "x : "+str(x)+"  y : "+str(y)+"  w : "+str(w)+"  h : "+str(h), (300, 50), 0, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
		'''
	'''
	cv2.imshow ("frame", frame)
	cv2.imshow ("frame_copy", frame_copy)
	key = cv2.waitKey(1) & 0xFF
	if key == ord("q"):
		break
	'''
	'''
	elif key is 's':
		cv2.imwrite ('extracted/'+str(num_capsule)+".bmp", copyImage)
		num_capsule += 1
	'''

cv2.destroyAllWindows()
