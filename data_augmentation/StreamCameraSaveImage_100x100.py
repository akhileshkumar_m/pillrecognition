import cv2
import argparse
import os
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=True,
	help="path to save the rotate images")
args = vars(ap.parse_args())


cam = cv2.VideoCapture (0)
if not cam.isOpened():
	print ('camera not opened')
	exit()
cam.set (cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set (cv2.CAP_PROP_FRAME_HEIGHT, 720)

final_h = 224
final_w = 224

num_capsule = 0
print ('For saving image press ')
print ('a : capsule\ni : circle\ne : ellipse')
while True:
	ret, image = cam.read()
	if ret is False:
		print ("image not found")
		exit()
	tempImage = image.copy()
	copyImage = image.copy()
	'''
	imgX, imgY = image.shape[1]/2, image.shape[0]/2
	image = image [imgY-final_h/2:imgY+final_h/2, imgX-final_w/2:imgX+final_w/2]
	tempImage [imgY+final_h/2, imgX-final_w/2:imgX+final_w/2] = 255
	tempImage [imgY-final_h/2, imgX-final_w/2:imgX+final_w/2] = 255
	tempImage [imgY-final_h/2:imgY+final_h/2, imgX+final_w/2] = 255
	tempImage [imgY-final_h/2:imgY+final_h/2, imgX-final_w/2] = 255
	cv2.imshow ('full image', tempImage)
	'''
	cv2.imshow ('image', image)
	k = cv2.waitKey(1)
	k = chr(k&255)
	if k is 'q':
		break
	elif k is 's':
		fileName_1 = os.path.sep.join([args["output"], str(num_capsule)+".bmp"])
		fileName_2 = os.path.sep.join([args["output"], str(num_capsule)+"_cropped.bmp"])
		print ('fileName_1 : %s'%fileName_1)
		cv2.imwrite (fileName_1, copyImage)
#		cv2.imwrite (fileName_2, image)
		num_capsule += 1
