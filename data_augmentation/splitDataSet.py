# USAGE
# python minivggnet_flowers17_data_aug.py --dataset ../datasets/flowers17/images --training ../dataset/flower17/training_set --validation ../dataset/flower17/validation_set

# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import MiniVGGNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-t", "--training", required=True,
	help="path to input dataset")
ap.add_argument("-v", "--validation", required=True,
	help="path to input dataset")		
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]

# initialize the image preprocessors


numberOfImage = 0

totalImages = len(imagePaths)
BATCH_SIZE = 1000
print ("totalImages : %d"%totalImages)
for start in range(0, totalImages, BATCH_SIZE):
	if (totalImages - start) < BATCH_SIZE:
		end = totalImages
	else:
		end = start + BATCH_SIZE
	print ("\nstart : %d\tend : %d"%(start, end))
	iap = ImageToArrayPreprocessor()
	sdl = SimpleDatasetLoader(preprocessors=[iap])
	(data, labels) = sdl.load(imagePaths[start:end], verbose=500)
	(trainX, testX, trainY, testY) = train_test_split(data, labels,	test_size=0.1, random_state=42)
	print ('trainX : ', trainX.shape)

	for ind in range(len(trainX)):
		cv2.imwrite ( os.path.sep.join([args["training"], trainY[ind], trainY[ind]+'_'+str(numberOfImage)+".bmp"]), trainX[ind])
		numberOfImage += 1


	for ind in range(len(testX)):
		cv2.imwrite ( os.path.sep.join([args["validation"], testY[ind], testY[ind]+'_'+str(numberOfImage)+".bmp"]), testX[ind])
		numberOfImage += 1
	del trainX, testX, trainY, testY, iap, sdl, data, labels

