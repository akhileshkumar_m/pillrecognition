# USAGE
# python minivggnet_flowers17_data_aug.py --dataset ../datasets/flowers17/images --training ../dataset/flower17/training_set --validation ../dataset/flower17/validation_set

# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import MiniVGGNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-t", "--training", required=True,
	help="path to input dataset")
ap.add_argument("-v", "--validation", required=True,
	help="path to input dataset")		
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]

# initialize the image preprocessors
#aap = AspectAwarePreprocessor(120, 120)
#aap = AspectAwarePreprocessor(224, 224)
iap = ImageToArrayPreprocessor()

# load the dataset from disk then scale the raw pixel intensities
# to the range [0, 1]


sdl = SimpleDatasetLoader(preprocessors=[ iap])
(data, labels) = sdl.load(imagePaths, verbose=500)

#(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, random_state=42)
	
(trainX, trainY) = data, labels
print ('trainX : ', trainX.shape)

# convert the labels from integers to vectors
#trainY = LabelBinarizer().fit_transform(trainY)

# construct the image generator for data augmentation
aug = ImageDataGenerator( channel_shift_range=0.1,
	fill_mode="nearest")
'''
	
aug = ImageDataGenerator(rotation_range=360, fill_mode='constant', cval=100,
			horizontal_flip=True, vertical_flip=True,
			channel_shift_range=0.2)
'''
numberOfImage = 0

for x_train_augmented, y_train_augmented in aug.flow(trainX, trainY, batch_size=len(trainX)):
	(ht, wt, dp) = x_train_augmented[0].shape
	for ind in range(len(x_train_augmented)):
	
		numberOfImage += 1
#		imageSave = x_train_augmented[ind].reshape ((ht, wt, dp))

#		cv2.imwrite (args["training"]+y_train_augmented[ind]+'/'+str(numberOfImage)+".bmp", x_train_augmented[ind])
#		cv2.imwrite ( os.path.sep.join([args["training"], y_train_augmented[ind], str(numberOfImage)+".bmp"]), x_train_augmented[ind])
		cv2.imwrite ( os.path.sep.join([args["training"], str(numberOfImage)+".bmp"]), x_train_augmented[ind])
		if numberOfImage == 2*10:
			break
	if numberOfImage == 2*10:
			break
			
'''
for ind in range(len(testX)):
	cv2.imwrite ( os.path.sep.join([args["validation"], testY[ind], str(numberOfImage)+".bmp"]), testX[ind])
	numberOfImage += 1
'''			
			
'''
for x_train_augmented, y_train_augmented in aug.flow(trainX, trainY, batch_size=1):

	numberOfImage += 1
	imageSave = x_train_augmented.reshape ((x_train_augmented.shape[1], x_train_augmented.shape[2], x_train_augmented.shape[3]))

	cv2.imwrite (args["output"]+y_train_augmented[0]+'/'+str(numberOfImage)+".bmp", imageSave)
	if numberOfImage == 1000:
		break
'''
