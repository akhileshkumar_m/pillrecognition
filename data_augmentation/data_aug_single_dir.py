# USAGE
#python data_aug_single_dir.py --training ../../dataset/pill_6/training_flip_aug/ --dataset ../../dataset/pill_6/training_cropped/ADVANCE/ 
# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import MiniVGGNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-t", "--training", required=True,
	help="path to input dataset")	
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]

prefix = imagePaths[0].split(os.path.sep)[-2]
print ('prefix : ', prefix)

# initialize the image preprocessors
#aap = AspectAwarePreprocessor(64, 64)
#aap = AspectAwarePreprocessor(224, 224)
iap = ImageToArrayPreprocessor()

# load the dataset from disk then scale the raw pixel intensities
# to the range [0, 1]


sdl = SimpleDatasetLoader(preprocessors=[iap])
(data, labels) = sdl.load(imagePaths, verbose=500)
trainX, trainY = data, labels

#aug = ImageDataGenerator(rotation_range=30, horizontal_flip=True, vertical_flip=True, fill_mode="nearest")
aug = ImageDataGenerator(horizontal_flip=True, vertical_flip=True)

numberOfImage = 0

for x_train_augmented, y_train_augmented in aug.flow(trainX, trainY, batch_size=len(trainX)):
	(ht, wt, dp) = x_train_augmented[0].shape
	for ind in range(len(x_train_augmented)):
	
		numberOfImage += 1
		cv2.imwrite ( os.path.sep.join([args["training"], prefix, prefix+'_'+str(numberOfImage)+".bmp"]), x_train_augmented[ind])
		if numberOfImage == 1000:
			break
	if numberOfImage == 1000:
			break


