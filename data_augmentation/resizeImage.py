# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.preprocessing import cropImages
from pyimagesearch.preprocessing import resizeImagesByN
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import MiniVGGNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("-o", "--output", required=True,
	help="path to input dataset")
args = vars(ap.parse_args())

# grab the list of images that we'll be describing, then extract
# the class label names from the image paths
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
'''
classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
classNames = [str(x) for x in np.unique(classNames)]
'''
#ri = resizeImagesByN (20)
imageCount = 10081
for pt in imagePaths:
	image = cv2.imread (pt)
	#resizedImage = ri.preprocess (image)
	resizedImage = cv2.resize(image, (100, 100), interpolation=cv2.INTER_LINEAR)
	savePath = pt.split(os.path.sep)
#	savePath = os.path.sep.join ( [args["output"], savePath[-2], savePath[-1]])
	savePath = os.path.sep.join ( [args["output"], savePath[-2], savePath[-2]+"_"+str(imageCount)+".bmp"])
	imageCount += 1
	cv2.imwrite (savePath, resizedImage)
	
