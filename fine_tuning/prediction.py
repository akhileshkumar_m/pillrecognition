# USAGE
# python rank_accuracy.py

# import the necessary packages
from config import pill_6_config as config
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import SimplePreprocessor
from pyimagesearch.preprocessing import MeanPreprocessor
from pyimagesearch.utils.ranked import rank5_accuracy
from pyimagesearch.io import HDF5DatasetGenerator
from keras.models import load_model
import json
import cv2
import argparse
from sklearn.metrics import classification_report
import numpy as np

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, required=True,
	help="path to *specific* model checkpoint to load")
ap.add_argument("-d", "--dataset_mean", type=str, required=True,
	help="path to dataset mean of rgb to load")
args = vars(ap.parse_args())	
	
classNames = [  'ADVANCE',
		'BACKGROUND',
		'CROCIN',
		'DART',
		'DIGENE',
		'DOLO_650',
		'GELUSIL_MPS',
		'PLAIN_WHITE_CAPSULE',
		'PLAIN_WHITE_ELLIPSE',
		'RED_CIRCULAR',
		'ROXIRITE_BLUE',
		'SCORED_WHITE_CAPSULE',
		'SCORED_WHITE_ELLIPSE',
		'SMALL_PINK_CIRCULAR',
		'SMALL_WHITE_CIRCULAR']
			
# load the RGB means for the training set
means = json.loads(open(args["dataset_mean"]).read())

# initialize the image preprocessors
#sp = SimplePreprocessor(64, 64)
mp = MeanPreprocessor(means["R"], means["G"], means["B"])
iap = ImageToArrayPreprocessor()

cam = cv2.VideoCapture(0)
if not cam.isOpened():
	print ("cam not found")
	exit()

# load the pre-trained network
print("[INFO] loading model...")
model = load_model(args["model"])


while True:
	canvas = np.zeros((35*len(classNames)+5, 300, 3), dtype="uint8")
	# initialize the testing dataset generator
#	testGen = HDF5DatasetGenerator(config.TEST_HDF5, 64, preprocessors=[mp, iap], classes=config.NUM_CLASSES)

	ret, frame = cam.read()
	frame_copy = frame.copy()
	frame_crop = frame[ int ( int(len(frame)/2) - 224/2) : int (int(len(frame)/2)  + 224/2 ) , int( int(len(frame[0])/2) - 224/2 ) : int( int(len(frame[0])/2) + 224/2) ]
	'''
	frame_crop = cv2.resize (frame_crop, (100, 100))
#	frame_crop = cv2.imread ('/home/akhileshkumar/python_program/pill_counting/pyImageSearchImplementation/dataset/pill_6/validation/SCORED_WHITE_CAPSULE/SCORED_WHITE_CAPSULE_8984.bmp')
	cv2.imshow ('crop frame', frame_crop)
	frame_crop = frame_crop.astype("float") / 255.0
	frame_crop = mp.preprocess(frame_crop)
	frame_crop = iap.preprocess(frame_crop)
	'''
	frame_crop = np.expand_dims(frame_crop, axis=0)
	predictions = model.predict_proba(frame_crop, batch_size=1)
	#print ('predictions : ', predictions)
	
	frame_copy[int (int(len(frame)/2)  + 224/2 ) , int( int(len(frame[0])/2) - 224/2 ) : int( int(len(frame[0])/2) + 224/2)] = 255
	frame_copy[ int ( int(len(frame)/2) - 224/2), int( int(len(frame[0])/2) - 224/2 ) : int( int(len(frame[0])/2) + 224/2)] = 255
	frame_copy[ int ( int(len(frame)/2) - 224/2) : int (int(len(frame)/2)  + 224/2 ) , int( int(len(frame[0])/2) + 224/2)] = 255
	frame_copy[ int ( int(len(frame)/2) - 224/2) : int (int(len(frame)/2)  + 224/2 ) , int( int(len(frame[0])/2) - 224/2 )] = 255
	
	for ind in range (len (predictions[0])):
		predictions[0][ind] = round (predictions[0][ind]*100, 2)
	
	
	pred = np.argmax(predictions, axis=1)
	for ind in range (len (predictions[0])):
		w = int (predictions[0][int(ind)]*3 )
#		cv2.putText(frame_copy, classNames[int(ind)]+' '+str(predictions[0][int(ind)]), (30, 50+30*ind), 0, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
		cv2.rectangle(canvas, (5, (ind * 35) + 5), (w+5, (ind * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, classNames[int(ind)]+' '+str(predictions[0][int(ind)]), (10, (ind * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

	
	cv2.putText(frame_copy, classNames[int(pred)]+' '+str(predictions[0][int(pred)]), (300, 50), 0, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
	
	cv2.imshow ('full frame', frame_copy)
	cv2.imshow ('Probabilities', canvas)	
	k = cv2.waitKey(1)
	if chr(k&255) is 'q':
		break
	
	'''
	# compute the rank-1 and rank-5 accuracies
	(rank1, rank5) = rank5_accuracy(predictions, testGen.db["labels"])
	print("[INFO] rank-1: {:.2f}%".format(rank1 * 100))
	print("[INFO] rank-5: {:.2f}%".format(rank5 * 100))

	# close the database
	testGen.close()
	'''
