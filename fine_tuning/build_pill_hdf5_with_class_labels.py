# USAGE
# python build_dogs_vs_cats.py

# import the necessary packages
from config import pill_13_config as config
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import json
import cv2
import os
from sklearn.utils import shuffle

# grab the paths to the images
le = LabelEncoder()

imagePaths = list(paths.list_images(config.TRAINING_IMAGES_PATH))
imagePaths = shuffle(imagePaths)
trainLabels = [pt.split(os.path.sep)[-2] for pt in imagePaths]
trainLabels = le.fit_transform(trainLabels)
print ('trainLabels : ', len(trainLabels))


val_imagePaths = list(paths.list_images(config.VALIDATION_IMAGES_PATH))
val_imagePaths = shuffle(val_imagePaths)
val_trainLabels = [pt.split(os.path.sep)[-2] for pt in val_imagePaths]
val_trainLabels = le.fit_transform(val_trainLabels)


#print ('trainLabels : ', trainLabels)
# construct a list pairing the training, validation, and testing
# image paths along with their corresponding labels and output HDF5
# files
datasets = [
	("train", imagePaths, trainLabels, config.TRAIN_HDF5),
	("val", val_imagePaths, val_trainLabels, config.VAL_HDF5),]

# initialize the image pre-processor and the lists of RGB channel
# averages
aap = AspectAwarePreprocessor(224, 224)
(R, G, B) = ([], [], [])

# loop over the dataset tuples
for (dType, paths, labels, outputPath) in datasets:
	# create HDF5 writer
	print("[INFO] building {}...".format(outputPath))
	writer = HDF5DatasetWriter((len(paths), 224, 224, 3), outputPath)
	writer.storeClassLabels(le.classes_)

	# initialize the progress bar
	widgets = ["Building Dataset: ", progressbar.Percentage(), " ",
		progressbar.Bar(), " ", progressbar.ETA()]
	pbar = progressbar.ProgressBar(maxval=len(paths),
		widgets=widgets).start()

	# loop over the image paths
	for (i, (path, label)) in enumerate(zip(paths, labels)):
		# load the image and process it
		image = cv2.imread(path)
#		print (image.shape)
		image = image.astype("float")/255.0

		# if we are building the training dataset, then compute the
		# mean of each channel in the image, then update the
		# respective lists
		if dType == "train":
			(b, g, r) = cv2.mean(image)[:3]
			R.append(r)
			G.append(g)
			B.append(b)

		# add the image and label # to the HDF5 dataset
		writer.add([image], [label])
		pbar.update(i)

	# close the HDF5 writer
	pbar.finish()
	writer.close()

# construct a dictionary of averages, then serialize the means to a
# JSON file
print("[INFO] serializing means...")
D = {"R": np.mean(R), "G": np.mean(G), "B": np.mean(B)}
f = open(config.DATASET_MEAN, "w")
f.write(json.dumps(D))
f.close()
