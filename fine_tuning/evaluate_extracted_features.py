# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from config import pill_13_config_extracted_feature as config
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
from pyimagesearch.preprocessing import ExtractPreprocessor
from keras.models import load_model



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--model", required=True,
	help="path to model")
args = vars(ap.parse_args())

# load the VGG16 network
print("[INFO] loading network...")
model = VGG16(weights="imagenet", include_top=False)

model_1 = load_model(args["model"])

fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels_str = pickle.load(f)
print ('classes : ', len (labels_str))
score = [None]*(len(labels_str))
print ('labels_str : ',labels_str)

test_dataset_length = 50

print ('config.TEST_HDF5 : {}'.format(config.TEST_HDF5))
db = h5py.File(config.TEST_HDF5, "r")
images = db["features"][:]
print ('len(images) : {}'.format(len(images)))
#exit()



classNames = [str(x) for x in np.unique(db["labels"][:])]
print ('classNames : {} {}'.format(classNames,len(classNames)))

classNames = [str(x) for x in np.unique(labels_str)]
print ('classNames : {} {}'.format(classNames, len(classNames)))

widgets = ["Building Dataset: ", progressbar.Percentage(), " ",
	progressbar.Bar(), " ", progressbar.ETA()]
pbar = progressbar.ProgressBar(maxval=len(images),
	widgets=widgets).start()

'''
features = []
number_iter = 0
for image in images:
	image = np.expand_dims(image, axis=0)
	feature = model.predict(image, batch_size=32, verbose=0)
	features.append(feature)
	number_iter += 1
	pbar.update(number_iter)
features = np.asarray(features)
print ('features.shape : {}'.format(features.shape))
features = features.reshape((-1, 512 * 7 * 7))
'''
features = images
preds = model_1.predict(features, verbose=0)


print ('preds : ', preds)
print ('db["labels"][:10] : ',len(db["labels"][:]))
print ('db["labels"][:].argmax(axis=0) : ', db["labels"][:])
#predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(db["labels"][:],
	preds.argmax(axis=1), target_names=labels_str))

#print(classification_report(db["labels"][:],
#	preds.argmax(axis=1)))
