# USAGE
# python build_dogs_vs_cats.py

# import the necessary packages
#from config import pill_13_config as config
from config import fineTune_pill_14_config_29_classes as config
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import json
import cv2
import os
from sklearn.utils import shuffle

# grab the paths to the images
le = LabelEncoder()

imagePaths = list(paths.list_images(config.VALIDATION_IMAGES_PATH))
imagePaths = shuffle(imagePaths)
testLabels = [pt.split(os.path.sep)[-2] for pt in imagePaths]
testLabels = le.fit_transform(testLabels)
print ('testLabels : ', len(testLabels))

#datasets = [("test", imagePaths, testLabels, config.TRAIN_HDF5)]
datasets = [("test", imagePaths, testLabels, config.VAL_HDF5)]

# loop over the dataset tuples
for (dType, paths, labels, outputPath) in datasets:
	# create HDF5 writer
	print("[INFO] building {}...".format(outputPath))
	writer = HDF5DatasetWriter((len(paths), 224, 224, 3), outputPath)
	writer.storeClassLabels(le.classes_)

	# initialize the progress bar
	widgets = ["Building Dataset: ", progressbar.Percentage(), " ",
		progressbar.Bar(), " ", progressbar.ETA()]
	pbar = progressbar.ProgressBar(maxval=len(paths),
		widgets=widgets).start()

	# loop over the image paths
	for (i, (path, label)) in enumerate(zip(paths, labels)):
		# load the image and process it
		image = cv2.imread(path)
		# add the image and label # to the HDF5 dataset
		writer.add([image], [label])
		pbar.update(i)

	# close the HDF5 writer
	pbar.finish()
	writer.close()
