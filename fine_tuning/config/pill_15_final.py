# define the paths to the images directory
#TRAINING_IMAGES_PATH = "../dataset/pill_14/fineTuning_29_classes/training/"
#/home/akhileshkumar/python_program/pill_counting/pyImageSearchImplementation/dataset/pill_6/training_augmented/hdf5
#VALIDATION_IMAGES_PATH = "../dataset/pill_14/fineTuning_29_classes/validation/"

# since we do not have validation data or access to the testing
# labels we need to take a number of images from the training
# data and use them instead
NUM_CLASSES = 4
#NUM_VAL_IMAGES = 200 * NUM_CLASSES
#NUM_TEST_IMAGES = 1250 * NUM_CLASSES

# define the path to the output training, validation, and testing
# HDF5 files

TRAIN_HDF5 = "../../../dataset/pill_15/extractedFeature/extractedFeature_VGG16_training.hdf5"
VAL_HDF5   = "../../../dataset/pill_15/extractedFeature/extractedFeature_VGG16_validation.hdf5"
#TRAIN_HDF5 = "../dataset/pill_15/extractedFeature/extractedFeature_VGG16_training.hdf5"
#VAL_HDF5   = "../dataset/pill_15/extractedFeature/extractedFeature_VGG16_validation.hdf5"
#TEST_HDF5  = "../dataset/pill_14/extractedFeatures/fineTuning/extractedFeature_VGG16_fineTuning_validation.hdf5"

# path to the output model file
#MODEL_PATH = "../dataset/pill_13/fine_tuning_finalImages/training/pill_6.model"

# define the path to the dataset mean
#DATASET_MEAN = "../dataset/pill_13/fine_tuning_finalImages/training/hdf5/dataset_mean.json"

# define the path to the output directory used for storing plots,
# classification reports, etc.
OUTPUT_PATH = "../../output"
