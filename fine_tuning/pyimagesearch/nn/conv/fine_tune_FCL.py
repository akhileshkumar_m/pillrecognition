# import the necessary packages
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers import Dense
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation
from keras import backend as K
from keras.layers import TimeDistributed
from keras.layers import Input

class fine_tune_FCL:
	@staticmethod
	def build(ext_input_shape, classes, D):
		model = Sequential()
		
		model.add(Dense(D, input_shape=(25088,)))
		model.add(Dropout(0.5))
		model.add(Dense(classes))
		model.add(Activation("softmax"))

		# return the constructed network architecture
		return model
