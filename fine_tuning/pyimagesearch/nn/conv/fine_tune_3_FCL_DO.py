# import the necessary packages
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers import Dense
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation
from keras import backend as K
from keras.layers import TimeDistributed
from keras.layers import Input

class fine_tune_3_FCL_DO:
	@staticmethod
	def build(ext_input_shape, classes,DO=0.8):
		model = Sequential()
		model.add(Dense(1024, activation='relu', input_shape=(25088,)))
		model.add(Dropout(DO))
		model.add(Dense(512, activation='relu'))
		model.add(Dropout(DO))
		model.add(Dense(classes))
		model.add(Activation("softmax"))

		# return the constructed network architecture
		return model
