# USAGE
# python train_alexnet.py

# import the necessary packages
# set the matplotlib backend so figures can be saved in the background
import matplotlib
matplotlib.use("Agg")

# import the necessary packages
from config import pill_13_config as config
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
#from pyimagesearch.preprocessing import SimplePreprocessor
#from pyimagesearch.preprocessing import PatchPreprocessor
from pyimagesearch.preprocessing import MeanPreprocessor
from pyimagesearch.callbacks import TrainingMonitor
from pyimagesearch.io import HDF5DatasetGenerator
#from pyimagesearch.nn.conv import AlexNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
import json
import os
import argparse
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
#from pyimagesearch.nn.conv import ShallowNet_DO_REG
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.utils import plot_model
from keras.callbacks import LearningRateScheduler
import keras.backend as K
from pyimagesearch.callbacks import EpochCheckpoint
from keras.models import load_model
from pyimagesearch.nn.conv import FCHeadNet
from keras.applications import VGG16
from pyimagesearch.nn.conv import FCHeadNet
from keras.optimizers import RMSprop
from keras.optimizers import SGD
from keras.layers import Input
from keras.models import Model



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str,
	help="path to *specific* model checkpoint to load")
ap.add_argument("-s", "--start_epoch", type=int, default=0,
	help="epoch to restart training at")
ap.add_argument("-c", "--checkpoints", required=True,
	help="path to output checkpoint directory")
args = vars(ap.parse_args())
NUM_EPOCHS = 20
INIT_LR = 1e-3
BATCH_SIZE = 1
LR_History = []
fileName = __file__.split(os.path.sep)[-1].split(".")[-2]
def poly_decay(epoch):
	# initialize the maximum number of epochs, base learning rate,
	# and power of the polynomial
	maxEpochs = NUM_EPOCHS
	baseLR = INIT_LR
	power = 1.0
	# compute the new learning rate based on polynomial decay
	alpha = baseLR * (1 - (epoch / float(maxEpochs))) ** power
	LR_History.append(alpha)
	# return the new learning rate
	return alpha

'''
# construct the training image generator for data augmentation
aug = ImageDataGenerator(rotation_range=20, zoom_range=0.15,
	width_shift_range=0.2, height_shift_range=0.2, shear_range=0.15,
	horizontal_flip=True, fill_mode="nearest")
'''
# load the RGB means for the training set
means = json.loads(open(config.DATASET_MEAN).read())

# initialize the image preprocessors
#sp = SimplePreprocessor(224, 224)
#pp = PatchPreprocessor(224, 224)
mp = MeanPreprocessor(means["R"], means["G"], means["B"])
iap = ImageToArrayPreprocessor()

# initialize the training and validation dataset generators
trainGen = HDF5DatasetGenerator(config.TRAIN_HDF5, BATCH_SIZE,
	preprocessors=[mp, iap], classes=config.NUM_CLASSES)
valGen = HDF5DatasetGenerator(config.VAL_HDF5, BATCH_SIZE,
	preprocessors=[mp, iap], classes=config.NUM_CLASSES)

# initialize the optimizer
print("[INFO] compiling model...")

# if there is no specific model checkpoint supplied, then initialize
# the network (ResNet-56) and compile the model
if args["model"] is None:
#	h, w = len (trainX[0]), len (trainX[1])
	h, w = 100, 100
	print("[INFO] compiling model...")
	# load the VGG16 network, ensuring the head FC layer sets are left
	# off
	baseModel = VGG16(weights="imagenet", include_top=False,
		input_tensor=Input(shape=(224, 224, 3)))

	# initialize the new head of the network, a set of FC layers
	# followed by a softmax classifier
	headModel = FCHeadNet.build(baseModel, config.NUM_CLASSES, 128)

	# place the head FC model on top of the base model -- this will
	# become the actual model we will train
	model = Model(inputs=baseModel.input, outputs=headModel)

	# loop over all layers in the base model and freeze them so they
	# will *not* be updated during the training process
	for layer in baseModel.layers:
		layer.trainable = False

	# compile our model (this needs to be done after our setting our
	# layers to being non-trainable
	print("[INFO] compiling model...")
	opt = Adam(lr=INIT_LR)
	model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
	'''
	opt = Adam(lr=INIT_LR)
	model = ShallowNet_DO_REG.build(width=w, height=h, depth=3, classes=config.NUM_CLASSES, DO=0.25, reg=0.0002)
	model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
	'''
	plot_model(model, to_file="output/network_"+fileName+".png", show_shapes=True)

# otherwise, load the checkpoint from disk
else:
	print("[INFO] loading {}...".format(args["model"]))
	model = load_model(args["model"])
	# update the learning rate
	print("[INFO] old learning rate: {}".format(
		K.get_value(model.optimizer.lr)))
	K.set_value(model.optimizer.lr, 1e-3)
	print("[INFO] new learning rate: {}".format(
		K.get_value(model.optimizer.lr)))
		
'''opt = Adam(lr=1e-3)
model = AlexNet.build(width=227, height=227, depth=3,
	classes=2, reg=0.0002)
model.compile(loss="binary_crossentropy", optimizer=opt,
	metrics=["accuracy"])
'''
# construct the set of callbacks
directoryName = os.path.sep.join([args["checkpoints"], fileName])
if not os.path.exists(directoryName):
    os.makedirs(directoryName)
'''
callbacks = [
	EpochCheckpoint(directoryName, every=5,
		startAt=args["start_epoch"]),
	TrainingMonitor("output/"+fileName+".png",
		jsonPath="output/"+fileName+".json",
		startAt=args["start_epoch"]),
	LearningRateScheduler(poly_decay)]
'''	

callbacks = [
	EpochCheckpoint(directoryName, every=5,
		startAt=args["start_epoch"]),
	TrainingMonitor("output/"+fileName+".png",
		jsonPath="output/"+fileName+".json",
		startAt=args["start_epoch"])]

'''
# construct the set of callbacks
path = os.path.sep.join([config.OUTPUT_PATH, "{}.png".format(
	os.getpid())])
callbacks = [TrainingMonitor(path)]
'''
# train the network
print ('trainGen.numImages, valGen.numImages : ', trainGen.numImages, valGen.numImages)
model.fit_generator(
	trainGen.generator(),
	steps_per_epoch=trainGen.numImages // BATCH_SIZE,
	validation_data=valGen.generator(),
	validation_steps=valGen.numImages // BATCH_SIZE,
	epochs=20,
	max_q_size=BATCH_SIZE * 2,
	callbacks=callbacks, verbose=1)
'''
H = model.fit(trainX, trainY, validation_data=(testX, testY),
	batch_size=32, epochs=NUM_EPOCHS, callbacks=callbacks, verbose=1)
'''

# evaluate the network
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=classNames))

# plot the training loss and accuracy

plt.style.use("ggplot")
plt.figure(1)
plt.plot(np.arange(0, NUM_EPOCHS), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_acc"], label="val_acc")
graphName = "Training Loss and Accuracy ["+str(args["start_epoch"])+" - " + str(args["start_epoch"]+100)+"]"
plt.title(graphName)
plt.xlabel("Epoch #")	
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig('output/'+fileName+'_3.png')
plt.show()

# save the model to file
print("[INFO] serializing model...")
#model.save(config.MODEL_PATH, overwrite=True)

# close the HDF5 datasets
trainGen.close()
valGen.close()
