# USAGE
# python finetune_flowers17.py --dataset ../datasets/flowers17/images \
# 	--model flowers17.model

# import the necessary packages
from config import fineTune_pill_14_config_29_classes as config
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import FCHeadNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop
from keras.optimizers import SGD
from keras.applications import VGG16
from keras.layers import Input
from keras.models import Model
from imutils import paths
import numpy as np
import argparse
import os
#from pyimagesearch.io import HDF5DatasetGeneratorFineTune
from pyimagesearch.io import HDF5DatasetGenerator

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-d", "--dataset", required=True,
#	help="path to input dataset")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
args = vars(ap.parse_args())

NUM_EPOCHS = 10
EPOCHS_2 = 10
INIT_LR = 5e-6
LR_2 = 1e-6
BATCH_SIZE = 10

#trainGen = HDF5DatasetGeneratorFineTune(config.TRAIN_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)
#valGen = HDF5DatasetGeneratorFineTune(config.VAL_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)

trainGen = HDF5DatasetGenerator(config.TRAIN_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)
valGen = HDF5DatasetGenerator(config.VAL_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)

# load the VGG16 network, ensuring the head FC layer sets are left
# off
#JELARI - include_top = False means we dont want FC layer of VGG16

baseModel = VGG16(weights="imagenet", include_top=False,
	input_tensor=Input(shape=(224, 224, 3)))

# initialize the new head of the network, a set of FC layers
# followed by a softmax classifier
headModel = FCHeadNet.build(baseModel, config.NUM_CLASSES, 256)

# place the head FC model on top of the base model -- this will
# become the actual model we will train
#JELARI - BASICALLY WE ARE JOINING BOTH CONVOLUTION(VGG16) + FC LAYER HERE. 
model = Model(inputs=baseModel.input, outputs=headModel)

# loop over all layers in the base model and freeze them so they
# will *not* be updated during the training process
for layer in baseModel.layers:
	layer.trainable = False

# compile our model (this needs to be done after our setting our
# layers to being non-trainable
print("[INFO] compiling model...")
opt = RMSprop(lr=INIT_LR)
#JELARI : CHECK WHAT 'accuracy' means..
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# train the head of the network for a few epochs (all other
# layers are frozen) -- this will allow the new FC layers to
# start to become initialized with actual "learned" values
# versus pure random
print("[INFO] training head...")
'''
model.fit_generator(aug.flow(trainX, trainY, batch_size=batch_size),
	validation_data=(testX, testY), epochs=10,
	steps_per_epoch=len(trainX) // batch_size, verbose=1)
'''
'''
callbacks = [
	EpochCheckpoint(directoryName, every=5,
		startAt=args["start_epoch"]),
	TrainingMonitor("output/"+fileName+".png",
		jsonPath="output/"+fileName+".json",
		startAt=args["start_epoch"])]
'''
model.fit_generator(
	trainGen.generator(),
	steps_per_epoch=trainGen.numImages // BATCH_SIZE,
	validation_data=valGen.generator(),
	validation_steps=valGen.numImages // BATCH_SIZE,
	epochs=NUM_EPOCHS,
	max_q_size=BATCH_SIZE * 2, verbose=1)

# evaluate the network after initialization
'''
print("[INFO] evaluating after initialization...")
predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=classNames))
'''
# now that the head FC layers have been trained/initialized, lets
# unfreeze the final set of CONV layers and make them trainable
for layer in baseModel.layers[15:]:
	layer.trainable = True

# for the changes to the model to take affect we need to recompile
# the model, this time using SGD with a *very* small learning rate
print("[INFO] re-compiling model...")
opt = SGD(lr=LR_2)
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# train the model again, this time fine-tuning *both* the final set
# of CONV layers along with our set of FC layers
print("[INFO] fine-tuning model...")
'''
model.fit_generator(aug.flow(trainX, trainY, batch_size=batch_size),
	validation_data=(testX, testY), epochs=10,
	steps_per_epoch=len(trainX) // batch_size, verbose=1)
'''

model.fit_generator(
	trainGen.generator(),
	steps_per_epoch=trainGen.numImages // BATCH_SIZE,
	validation_data=valGen.generator(),
	validation_steps=valGen.numImages // BATCH_SIZE,
	epochs=EPOCHS_2,
	max_q_size=BATCH_SIZE * 2, verbose=1)

print("[INFO] serializing model...")
model.save(args["model"])

# evaluate the network on the fine-tuned model
print("[INFO] evaluating after fine-tuning...")
predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=classNames))

# save the model to disk
print("[INFO] serializing model...")
model.save(args["model"])
