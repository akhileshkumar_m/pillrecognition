# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech101/images \
# 	--output ../datasets/caltech101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5

# import the necessary packages
from keras.applications import VGG16
from keras.applications import imagenet_utils
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import cv2
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py
from pyimagesearch.preprocessing import ExtractPreprocessor
from keras.models import load_model



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-m", "--model", required=True,
	help="path to model")
ap.add_argument("-o", "--output", default=None,
	help="path to directory where the images should be saved")
ap.add_argument("-r", "--ref", default=None,
	help="path to reference images to show while prediction")
ap.add_argument("-b", "--baseImage", required=True,
	help="path to base image for the testing dataset")
ap.add_argument("-d", "--dataset", required=True,
	help="path to the parent directory of the test images")

args = vars(ap.parse_args())

crop_w = 224
crop_h = 224

MARGIN = 50

# load the VGG16 network
print("[INFO] loading network...")
model_1 = load_model(args["model"])

# load class labels
fileName = args["model"].split(".")
print ('fileName[-2]  : ',fileName[-2])
dirName = fileName[-2].split(os.path.sep)
dirName[-1] = "ClassLabels"
dirName = os.path.sep.join(dirName)
print ('dirName : ',dirName)
#fileName [-1] = "cpickle"
fileName = ".".join([dirName, "cpickle"])

f = open(fileName, "rb")
labels = pickle.load(f)
print ('classes : ', len (labels))
score = [None]*(len(labels))
print ('labels : ',labels)
f.close()

#loading dataset for reference images
refImages = [None]*(len(labels))
refImgCount = 0
for lb in labels:
	refPath = os.path.sep.join([args['ref'], lb])
	refImgPath = list(paths.list_images(refPath))
	refImages[refImgCount] = cv2.imread(refImgPath[0])
	refImgCount += 1

imageCount = 0

# load base image
image = cv2.imread (args["baseImage"])
imgX, imgY = image.shape[1]/2, image.shape[0]/2
croppedImage = image
first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)

# load the test images with the labels
le = LabelEncoder()
testImagePaths = list(paths.list_images(args["dataset"]))
groundTruthLabels = [pt.split(os.path.sep)[-2] for pt in testImagePaths]
groundTruthLabels = le.fit_transform(groundTruthLabels)
print ('trainLabels : ', len(groundTruthLabels))

'''
cv2.namedWindow ('croppedImage', )
cv2.namedWindow ('probabilities', )
cv2.namedWindow ('frame', cv2.WINDOW_NORMAL)
cv2.namedWindow ('reference image', )
'''
tempImage = image.copy()

tempImage[int(imgY)-int(crop_h/2), int(imgX)-int(crop_w/2):int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)+int(crop_h/2), int(imgX)-int(crop_w/2):int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)-int(crop_h/2):int(imgY)+int(crop_h/2), int(imgX)+int(crop_w/2)] = 0
tempImage[int(imgY)-int(crop_h/2):int(imgY)+int(crop_h/2), int(imgX)-int(crop_w/2)] = 0

def fineTunePrediction (image):
	fullImage = image.copy()

	status, extractedImage = extImage.preprocess (image)
	cv2.imshow("extractedImage", extractedImage)

	image = extractedImage.copy()
	canvas = np.zeros((35*20+5, 350*2, 3), dtype="uint8")
	croppedImage = image.copy()
	image = np.expand_dims(image, axis=0)

	features = image
	preds = model_1.predict(features, verbose=0)
	return preds
	pred = np.argmax(preds, axis=1)
	for cl in range(len(labels)):
		val = preds[0][cl]*100
		score[cl] = round(val, 2)
	best_score = score[int(pred)]

	for cl in range(len(labels)):
		w = int (score[cl]*3 )
		if cl == pred:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (255, 0, 0), -1)
		else:
			cv2.rectangle(canvas, (5+350*int(cl/20), (int(cl%20) * 35) + 5), (w+5+350*int(cl/20), (int(cl%20) * 35) + 35), (0, 0, 255), -1)
		cv2.putText(canvas, labels[int(cl)]+' '+str(score[cl]), (10+400*int(cl/20), (int(cl%20) * 35) + 23), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)
	cv2.putText(tempImage, labels[int(pred)]+' '+str(best_score), (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

	cv2.imshow ('croppedImage', croppedImage)
	cv2.imshow ('probabilities', canvas)
	cv2.imshow ('frame', tempImage)
	cv2.resizeWindow ('frame', int(1280/4), int(720/4))
	cv2.imshow ('reference image', refImages[int(pred)])
	k = cv2.waitKey(1)
	#return preds
	if chr(k&255) is 'q':
		return exit()
	elif chr(k&255) is 's':
		if args['output'] is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_full.bmp"]),        fullImage)
			#if extractedImage is not None:
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+".bmp"]),             tempImage)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_probability.bmp"]), canvas)
			cv2.imwrite (os.path.sep.join([args['output'], str(imageCount)+"_cropped.bmp"]),     croppedImage)
			imageCount += 1


widgets = ["Building Dataset: ", progressbar.Percentage(), " ",
	progressbar.Bar(), " ", progressbar.ETA()]
pbar = progressbar.ProgressBar(maxval=len(testImagePaths),
	widgets=widgets).start()
preds = []
numberOfImages = 0
for TIP in testImagePaths:
	image = cv2.imread (TIP)
	preds.append (fineTunePrediction (image))
	numberOfImages += 1
	pbar.update(numberOfImages)
	#preds.append (model_1.predict(image, verbose=0))
pbar.finish()
#print ('preds : ', preds)

#print ('db["labels"][:10] : ',len(db["labels"][:]))
#print ('db["labels"][:].argmax(axis=0) : ', db["labels"][:])

print(classification_report(groundTruthLabels,
	np.asarray(preds).argmax(axis=2), target_names=labels))
'''
print (np.asarray(groundTruthLabels))
print(classification_report(groundTruthLabels,
	np.asarray(preds).argmax(axis=2)))
'''
#f.close()
