# USAGE
# python train_alexnet.py

# import the necessary packages
# set the matplotlib backend so figures can be saved in the background
import matplotlib
matplotlib.use("Agg")

# import the necessary packages
from config import pill_14_config_extracted_feature as config
from pyimagesearch.preprocessing import ImageToArrayPreprocessor
from pyimagesearch.preprocessing import SimplePreprocessor
from pyimagesearch.preprocessing import PatchPreprocessor
from pyimagesearch.preprocessing import MeanPreprocessor
from pyimagesearch.callbacks import TrainingMonitor
from pyimagesearch.io import HDF5DatasetGeneratorFineTune
#from pyimagesearch.nn.conv import AlexNet
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
import json
import os
import argparse
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from pyimagesearch.preprocessing import AspectAwarePreprocessor
from pyimagesearch.datasets import SimpleDatasetLoader
from pyimagesearch.nn.conv import fine_tune_FCL
from keras.optimizers import Adam
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
from keras.callbacks import ModelCheckpoint
from keras.utils import plot_model
from keras.callbacks import LearningRateScheduler
import keras.backend as K
from pyimagesearch.callbacks import EpochCheckpoint
from pyimagesearch.callbacks import TrainingMonitor
from keras.models import load_model
import pickle



# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str,
	help="path to *specific* model checkpoint to load")
ap.add_argument("-s", "--start_epoch", type=int, default=0,
	help="epoch to restart training at")
ap.add_argument("-c", "--checkpoints", required=True,
	help="path to output checkpoint directory")
args = vars(ap.parse_args())
NUM_EPOCHS = 100
INIT_LR = 1e-5
BATCH_SIZE = 32
LR_History = []
fileName = __file__.split(os.path.sep)[-1].split(".")[-2]
def poly_decay(epoch):
	# initialize the maximum number of epochs, base learning rate,
	# and power of the polynomial
	maxEpochs = NUM_EPOCHS
	baseLR = INIT_LR
	power = 1.0
	# compute the new learning rate based on polynomial decay
	alpha = baseLR * (1 - (epoch / float(maxEpochs))) ** power
	LR_History.append(alpha)
	# return the new learning rate
	return alpha


# load the RGB means for the training set
#means = json.loads(open(config.DATASET_MEAN).read())

# initialize the image preprocessors
#sp = SimplePreprocessor(227, 227)
#pp = PatchPreprocessor(227, 227)
#mp = MeanPreprocessor(means["R"], means["G"], means["B"])
'''
iap = ImageToArrayPreprocessor()

# initialize the training and validation dataset generators
trainGen = HDF5DatasetGeneratorFineTune(config.TRAIN_HDF5, BATCH_SIZE,
	preprocessors=[iap], classes=config.NUM_CLASSES)
valGen = HDF5DatasetGeneratorFineTune(config.VAL_HDF5, BATCH_SIZE,
	preprocessors=[iap], classes=config.NUM_CLASSES)
'''

# initialize the training and validation dataset generators
trainGen = HDF5DatasetGeneratorFineTune(config.TRAIN_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)
valGen = HDF5DatasetGeneratorFineTune(config.VAL_HDF5, BATCH_SIZE, classes=config.NUM_CLASSES)


print("[INFO] compiling model...")

if args["model"] is None:
	h, w = 100, 100
	print("[INFO] compiling model...")
	opt = Adam(lr=INIT_LR)
	#model = fine_tune_FCL.build((1,25088), config.NUM_CLASSES, 256)
	model = fine_tune_FCL.build((25088), config.NUM_CLASSES, 1024)
	model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
	plot_model(model, to_file="output/network_"+fileName+".png", show_shapes=True)

else:
	pass

# construct the set of callbacks
directoryName = os.path.sep.join([args["checkpoints"], fileName])
if not os.path.exists(directoryName):
    os.makedirs(directoryName)

f = open(os.path.sep.join([directoryName, 'ClassLabels.cpickle']), "wb")
f.write(pickle.dumps(trainGen.labels_map[:]))
f.close()

'''
callbacks = [
	EpochCheckpoint(directoryName, every=5,
		startAt=args["start_epoch"]),
	TrainingMonitor("output/"+fileName+".png",
		jsonPath="output/"+fileName+".json",
		startAt=args["start_epoch"])]
'''
callbacks = [
	EpochCheckpoint(directoryName, every=5,
		startAt=args["start_epoch"]),
	TrainingMonitor("output/"+fileName+".png",
		jsonPath="output/"+fileName+".json",
		startAt=args["start_epoch"]),
	LearningRateScheduler(poly_decay)]

# train the network

model.fit_generator(
	trainGen.generator(),
	steps_per_epoch=trainGen.numImages // BATCH_SIZE,
	validation_data=valGen.generator(),
	validation_steps=valGen.numImages // BATCH_SIZE,
	epochs=NUM_EPOCHS,
	max_q_size=BATCH_SIZE * 2,
	callbacks=callbacks, verbose=1)
'''
H = model.fit(trainX, trainY, validation_data=(testX, testY),
	batch_size=BATCH_SIZE, epochs=NUM_EPOCHS, callbacks=callbacks, verbose=1)
'''

# evaluate the network
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size=BATCH_SIZE)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=classNames))

# plot the training loss and accuracy

plt.style.use("ggplot")
plt.figure(1)
plt.plot(np.arange(0, NUM_EPOCHS), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_acc"], label="val_acc")
graphName = "Training Loss and Accuracy ["+str(args["start_epoch"])+" - " + str(args["start_epoch"]+100)+"]"
plt.title(graphName)
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig('output/'+fileName+'_3.png')
plt.show()

# save the model to file
print("[INFO] serializing model...")
#model.save(config.MODEL_PATH, overwrite=True)

# close the HDF5 datasets
trainGen.close()
valGen.close()
